import { protractor, browser } from 'protractor';
import { serenity } from 'serenity-js';
import { Actor, BrowseTheWeb, Open } from 'serenity-js/lib/screenplay-protractor';

import { schedulePageActions, scheduleContextMenuOptions, scheduleHappeningModalActions, newPatientAppointmentActions } from './page-objects/schedule';
import { loginPageActions } from './page-objects/login';
import { generalElementsActions } from './page-objects/general';
import { patientsPageActions, profilePageActions, ledgerPageActions } from './page-objects/patients';
import { quotesPageActions, newQuoteActons } from './page-objects/patients/quotes-page-actions';
import { newQuotesElements } from './page-objects/patients/ui/quotes-page';
import { writeNotesPageActions } from './page-objects/patients/clinical/write-notes-page-actions';
import { readNotesPageActions } from './page-objects/patients/clinical/read-notes-page-actions';
import { medicalHistoryPageActions } from './page-objects/patients/clinical/medical-history-page-actions';
import { moneyPageActions, emailFormActions } from './page-objects/money';
var randomstring = require("randomstring");
var id;
// describe represents a suite
describe('Stage4md Scenarios ->', function () {
    this.timeout(600 * 1000);

    const stage = serenity.callToStageFor({
        actor: name => Actor.named(name).whoCan(BrowseTheWeb.using(protractor.browser)),
    });

    before(() => {
        // browser.driver.manage().window().maximize();
        id = randomstring.generate({
            length: 10,
            charset: 'alphabetic'
        });

    })

    afterEach(async () => {
        await browser.manage().deleteAllCookies();
        await browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();')
    })
    beforeEach(() => {

        return stage.theActorInTheSpotlight().attemptsTo(
            Open.browserOn("https://qa.stage4md.com"),
        )
    });

    // TODO add support to check email
    // it('User tries to add product to cart and checkout ', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickTheMoneyButton(),
    //         patientsPageActions.enterTextInSearchField("Damjan"),
    //         moneyPageActions.addProductToCartById("Botox"),
    //         moneyPageActions.clickGoToCartButton(),
    //         moneyPageActions.clickCreditCardButton(),
    //         moneyPageActions.clickCompleteSaleButton(),
    //         moneyPageActions.clickEmailReceiptButton(),
    //         emailFormActions.clickEmailSendButton(),
    //     ));


    // it('User tries to create chart note', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePatientsButton(),
    //         patientsPageActions.enterTextInSearchField("Damjan"),
    //         patientsPageActions.hoverOverTabButton("clinical"),
    //         patientsPageActions.clickButtonFromDropdownMenu("write notes"),
    //         writeNotesPageActions.clickEmailInstaNoteButton(),
    //         writeNotesPageActions.clickSave_note_Button()
    //     ));

    // it('User tries to add medical history ', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePatientsButton(),
    //         patientsPageActions.enterTextInSearchField("Damjan"),
    //         patientsPageActions.hoverOverTabButton("clinical"),
    //         patientsPageActions.clickButtonFromDropdownMenu("medical history"),
    //         medicalHistoryPageActions.clickDrugAllergiesYesButton(),
    //         medicalHistoryPageActions.clickPlusDrugAllergyButton(),
    //         medicalHistoryPageActions.enterDrugAllergy("as"),
    //         medicalHistoryPageActions.clickOnAsmanexOption(),
    //         medicalHistoryPageActions.clickMedicationsYesButton(),
    //         medicalHistoryPageActions.clickPlusMedicationsButton(),
    //         medicalHistoryPageActions.enterMedication("as"),
    //         medicalHistoryPageActions.clickOnAsmanexOption(),
    //         medicalHistoryPageActions.clickPastMedicalHistoryYesButton(),
    //         medicalHistoryPageActions.clickPlusMedicalHistoryButton(),
    //         medicalHistoryPageActions.enterPastMedicalHistory("as"),
    //         medicalHistoryPageActions.clickHodkinsDiseaseOption(),
    //         generalElementsActions.hardWait(10000),
    //         medicalHistoryPageActions.clickSurgicalHistoryYesButton(),
    //         medicalHistoryPageActions.clickPlusSurgicalHistoryButton(),
    //         medicalHistoryPageActions.enterSurgicalHistory("as"),
    //         medicalHistoryPageActions.clickOnblepharoplOption(),
    //         medicalHistoryPageActions.clickSaveTopButton()
    //     ));



    // it('User tries to create an existing appointment', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.filterSchedule("James"),
    //         schedulePageActions.rightClickOnCalendar("10am"),
    //         scheduleContextMenuOptions.clickTheNewPatientButton(),
    //         newPatientAppointmentActions.enterPatientName(id),
    //         newPatientAppointmentActions.enterPatientSurname(id),
    //         newPatientAppointmentActions.clickNextButton(),
    //         newPatientAppointmentActions.clickAlreadySeenButton(),
    //         newPatientAppointmentActions.enterPatientDob("8/13/1992"),
    //         newPatientAppointmentActions.clickMaleGenderButton(),
    //         newPatientAppointmentActions.clickManualEntryAddressButton(),
    //         newPatientAppointmentActions.enterAddress("Somwhere"),
    //         newPatientAppointmentActions.enterCity("Somwhere"),
    //         // newPatientAppointmentActions.selectState("AK"),
    //         newPatientAppointmentActions.enterPostCode("55609"),
    //         newPatientAppointmentActions.enterCountryCode("US"),
    //         newPatientAppointmentActions.enterPhone("1111111111"),
    //         newPatientAppointmentActions.enterEmail(id + "@gmail.com"),
    //         newPatientAppointmentActions.selectReferrer("unknown"),
    //         newPatientAppointmentActions.clickSaveButton(),
    //         newPatientAppointmentActions.enterConcern("SomeConcern"),
    //         newPatientAppointmentActions.clickNewPatientAppointmentSaveButton(),
    //         schedulePageActions.checkIfNewPatientAppointmentWasCreated(id)
    //     ));

});