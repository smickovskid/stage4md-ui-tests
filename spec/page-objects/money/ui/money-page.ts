import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const moneyPageElements = {
    Go_to_cart_button: Target.the('Go to cart button').located(by.id('btnPayment')),
    Sales_button: Target.the('Sales button').located(by.xpath('//a[contains(text(),"Sales")]')),
    Receipts_button: Target.the('Receipts button').located(by.xpath('//a[contains(text(),"Receipts")]')),

    // Checkout elements
    Credit_card_button: Target.the('Credit card button').located(by.xpath('//input[@value="Credit Card"]')),
    Care_credit_button: Target.the('Care credit button').located(by.xpath('//input[@value="CareCredit"]')),
    Check_button: Target.the('Check button').located(by.xpath('//input[@value="Check"]')),
    Cash_button: Target.the('Cash button').located(by.xpath('//input[@value="Cash"]')),
    Complete_sale_button: Target.the('Complete sale button').located(by.id('btnprocess')),
    Print_receipt_button: Target.the('Print receipt button').located(by.id('btnprintreceipt')),
    Email_receipt_button: Target.the('Email receipt button').located(by.id('btnemailreceipt')),
    Close_receipt_button: Target.the('Close receipt button').located(by.id('btnclosereceipt')),
    Shopping_cart_loader: Target.the('Close receipt button').located(by.xpath('//table[@id="CartRegisterGrid_LPV"]')),



};


export const emailFormElements = {
    Email_field: Target.the('Email ield').located(by.id('emailto')),
    Send_email_button: Target.the('Send email button').located(by.id('sendemail')),
    Cancel_email_button: Target.the('Cancel email button').located(by.xpath('//button[@class="btn btn-outlinered pull-right button-cancel"]')),
}


