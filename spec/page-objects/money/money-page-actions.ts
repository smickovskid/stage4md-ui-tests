import { Scroll, See, Task, Text, Wait, Is, Enter, BrowseTheWeb, Target, Click } from 'serenity-js/lib/screenplay-protractor';

import { moneyPageElements, emailFormElements } from './ui/money-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { by } from 'protractor';
import { checkIfLoaderAppears, checkIfCartLoaderAppears, generalElementsActions } from '../general';
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { waitTime } from '../patients/clinical/medical-history-page-actions';
chai.use(chaiAsPromised);

export const emailFormActions = {


    enterRecipientEmail: (text: string) => Task.where(`{0} enters ${text} as email recipient`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(emailFormElements.Email_field, Is.visible()),
        Scroll.to(emailFormElements.Email_field),
        Enter.theValue(text).into(emailFormElements.Email_field)
    ),


    clickEmailSendButton: () => Task.where(`{0} clicks the email send button`,

        Wait.upTo(waitTime).until(emailFormElements.Send_email_button, Is.clickable()),
        Scroll.to(emailFormElements.Send_email_button),
        Click.on(emailFormElements.Send_email_button)
    ),


    clickEmailCancelButton: () => Task.where(`{0} clicks the email cancel button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(emailFormElements.Cancel_email_button, Is.clickable()),
        Scroll.to(emailFormElements.Cancel_email_button),
        Click.on(emailFormElements.Cancel_email_button)
    ),

}


export const moneyPageActions = {


    addProductToCartById: (text: string) => {
        let elem = Target.the(`${text} product`).located(by.xpath(`//div[@class="caption"]/h4[contains(text(),"${text}")]/parent::*//button[@id="AddCart"]`))
        return Task.where(`{0} adds the ${text} product to cart`,
        waitForAllTheModalsToBeAbsent(),    
        checkIfCartLoaderAppears(),
            generalElementsActions.waitForShoppingCartLoaderToDisappear(), waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },

    editProductFromCartByName: (text: string) => {
        let elem = Target.the(`${text} product in the cart`).located(by.xpath(`//table[@id="CartRegisterGrid"]//td[contains(text(),"${text}")]/parent::*//img[@title="Edit"]`))
        return Task.where(`{0} edits the ${text} product from cart`,
        waitForAllTheModalsToBeAbsent(),    
        checkIfCartLoaderAppears(),
            generalElementsActions.waitForShoppingCartLoaderToDisappear(), waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },


    deleteProductFromCartByName: (text: string) => {
        let elem = Target.the(`${text} product in the cart`).located(by.xpath(`//table[@id="CartRegisterGrid"]//td[contains(text(),"${text}")]/parent::*//img[@title="Delete"]`))
        return Task.where(`{0} deletes the ${text} product from cart`,
        waitForAllTheModalsToBeAbsent(),    
        checkIfCartLoaderAppears(),
            
            generalElementsActions.waitForShoppingCartLoaderToDisappear(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },



    clickGoToCartButton: () => Task.where(`{0} clicks the Go to cart button`,
        checkIfCartLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        generalElementsActions.waitForShoppingCartLoaderToDisappear(),
        Wait.upTo(waitTime).until(moneyPageElements.Go_to_cart_button, Is.clickable()),
        Scroll.to(moneyPageElements.Go_to_cart_button),
        Click.on(moneyPageElements.Go_to_cart_button)
    ),

    clickSalesButton: () => Task.where(`{0} clicks the Sales button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Sales_button, Is.clickable()),
        Scroll.to(moneyPageElements.Sales_button),
        Click.on(moneyPageElements.Sales_button)
    ),

    clickReceiptsButton: () => Task.where(`{0} clicks the Receipts button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Receipts_button, Is.clickable()),
        Scroll.to(moneyPageElements.Receipts_button),
        Click.on(moneyPageElements.Receipts_button)
    ),

    clickCreditCardButton: () => Task.where(`{0} clicks the Credit card button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Credit_card_button, Is.clickable()),
        Scroll.to(moneyPageElements.Credit_card_button),
        Click.on(moneyPageElements.Credit_card_button)
    ),

    clickCareCreditButton: () => Task.where(`{0} clicks the Care credit button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Care_credit_button, Is.clickable()),
        Scroll.to(moneyPageElements.Care_credit_button),
        Click.on(moneyPageElements.Care_credit_button)
    ),

    clickCheckButton: () => Task.where(`{0} clicks the Check button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Check_button, Is.clickable()),
        Scroll.to(moneyPageElements.Check_button),
        Click.on(moneyPageElements.Check_button)
    ),

    clickCashButton: () => Task.where(`{0} clicks the Cash button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Cash_button, Is.clickable()),
        Scroll.to(moneyPageElements.Cash_button),
        Click.on(moneyPageElements.Cash_button)
    ),

    clickCompleteSaleButton: () => Task.where(`{0} clicks the Complete sale button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Complete_sale_button, Is.clickable()),
        Scroll.to(moneyPageElements.Complete_sale_button),
        Click.on(moneyPageElements.Complete_sale_button)
    ),

    clickPrintReceiptButton: () => Task.where(`{0} clicks the Print receipt button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Print_receipt_button, Is.clickable()),
        Scroll.to(moneyPageElements.Print_receipt_button),
        Click.on(moneyPageElements.Print_receipt_button)
    ),

    clickEmailReceiptButton: () => Task.where(`{0} clicks the Email receipt button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Email_receipt_button, Is.clickable()),
        Scroll.to(moneyPageElements.Email_receipt_button),
        Click.on(moneyPageElements.Email_receipt_button)
    ),

    clickCloseReceiptButton: () => Task.where(`{0} clicks the Close receipt button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(moneyPageElements.Close_receipt_button, Is.clickable()),
        Scroll.to(moneyPageElements.Close_receipt_button),
        Click.on(moneyPageElements.Close_receipt_button)
    ),
}

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
