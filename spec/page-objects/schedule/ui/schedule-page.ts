import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by, element } from 'protractor';

export const schedulePage = {

    // Schedule header elements
    Stretch_Shrink_Button: Target.the('Stretch/Shrink calendar button').located(by.xpath('//a[@data-original-title="Stretch/shrink calendar"]')),
    Print_Schedule_Button: Target.the('Print schedule button').located(by.xpath('//a[@data-original-title="Print schedule"]')),
    Move_Up_List_Button: Target.the('Move up list button').located(by.xpath('//a[@data-original-title="Move-up list"]')),
    Refresh_Schedule_Button: Target.the('Refresh schedule button').located(by.xpath('//a[@data-original-title="Refresh"]')),
    Show_Inactive_Button: Target.the('Show Inactive appointments button').located(by.id('showinactiveappts')),
    Search_Appointments_Field: Target.the('Search appointments field').located(by.xpath('//input[@placeholder="Search Appointments.."]')),
    Search_Appointments_Button: Target.the('Search appointments button').located(by.xpath('//div[@class="search-appointments input-group add-on"]//button')),

    // Sidebar Calendar elements
    Left_Arrow_Sidebar_Calendar_Button: Target.the('Left arrow sidebar calendar button').located(by.xpath('//img[@class="dxEditors_edtCalendarPrevMonth_Moderno"]')),
    Right_Arrow_Sidebar_Calendar_Button: Target.the('Right arrow sidebar calendar button').located(by.xpath('//img[@class="dxEditors_edtCalendarNextMonth_Moderno"]')),
    Today_Sidebar_Calendar_Button: Target.the('Today sidebar calendar button').located(by.id('dateNavigatorScheduler_cal_BT')),

    // Calendar context menu button
    New_patient_button: Target.the('New patient button').located(by.xpath('//div[@id="scheduler_viewMenuBlock_SMVIEW"]//span[text()="New Patient"]')),
    Existing_patient_button: Target.the('Existing patient button').located(by.xpath('//div[@id="scheduler_viewMenuBlock_SMVIEW"]//span[text()="Existing Patient"]')),
    Happening_button: Target.the('Happening button').located(by.xpath('//div[@id="scheduler_viewMenuBlock_SMVIEW"]//span[text()="Happening"]')),
    Paste_button: Target.the('Paste button').located(by.xpath('//div[@id="scheduler_viewMenuBlock_SMVIEW"]//span[text()="Paste"]')),

    // Happening modal elements
    Happening_Subject_field: Target.the('Happening Subject field').located(by.id('subjecttb')),
    Happening_Comments_field: Target.the('Happening Comments field').located(by.id('apptdetail')),
    Happening_All_day_checkbox: Target.the('Happening All day checkbox').located(by.id('cbAllDay')),
    Happening_Recurrence_checkbox: Target.the('Happening Recurrence checkbox').located(by.id('appointmentRecurrenceForm_ChkRecurrence_S_D')),
    Save_happening_button: Target.the('Save happening button').located(by.id('savehappeningapptbtn')),
    Cancel_happening_button: Target.the('Cancel happening button').located(by.id('exitapptcancelbtnclear')),
    Appointment_type_dropdwon: Target.the('Appointment type dropdown').located(by.id('appttypes_B-1')),
    Happening_type_option: Target.the('Happening type option').located(by.xpath('//td[contains(text(), "Happening")]')),
    Happening_type_field: Target.the('Happening type field').located(by.id('appttypes_I')),


    // Appointment context menu elements
    Context_menu_edit_button: Target.the('Context menu edit button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Edit"]/parent::*')),
    Context_menu_copy_button: Target.the('Context menu copy button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Copy"]/parent::*')),
    Context_menu_reschedule_button: Target.the('Context menu reschedule button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Reschedule"]/parent::*')),
    Context_menu_cancel_button: Target.the('Context menu cancel button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Cancel"]/parent::*')),
    Context_menu_go_to_money_button: Target.the('Context menu go to money button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Go To Money"]/parent::*')),
    Context_menu_status_button: Target.the('Context menu status button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Status"]/parent::*')),
    Context_menu_history_button: Target.the('Context menu history button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="History"]/parent::*')),
    Context_menu_paste_button: Target.the('Context menu paste button').located(by.xpathSingleElement('//div[@id="scheduler_viewMenuBlock_SMVIEW_DXI3_T"]//span[text()="Paste"]/parent::*')),


    // Sub menu elements
    Context_menu_reschedule_by_office_button: Target.the('Context reschedule by office button').located(by.xpathSingleElement('//span[contains(text(),"By Office")]/parent::*')),
    Context_menu_reschedule_by_patient_button: Target.the('Context menu reschedule by patient button').located(by.xpathSingleElement('//span[contains(text(),"By Patient")]/parent::*')),
    Context_menu_cancel_by_office_button: Target.the('Context cancel by office button').located(by.xpathSingleElement('//span[contains(text(),"By Office")]/parent::*', null, 2)),
    Context_menu_cancel_by_patient_button: Target.the('Context menu cancel by patient button').located(by.xpathSingleElement('//span[contains(text(),"By Patient")]/parent::*', null, 2)),
    Context_menu_status_arrived_button: Target.the('Context menu status arrived button').located(by.xpathSingleElement('//span[contains(text(),"Arrived")]/parent::*')),
    Context_menu_status_in_room_button: Target.the('Context menu in room button').located(by.xpathSingleElement('//span[contains(text(),"In Room")]/parent::*')),
    Context_menu_status_seen_button: Target.the('Context menu seen button').located(by.xpathSingleElement('//span[contains(text(),"Seen")]/parent::*')),
    Context_menu_status_completed: Target.the('Context menu completed button').located(by.xpathSingleElement('//span[contains(text(),"Completed")]/parent::*')),
    Context_menu_status_left_unseen_button: Target.the('Context menu left unseen button').located(by.xpathSingleElement('//span[contains(text(),"Left Unseen")]/parent::*')),
    Context_menu_status_no_show_button: Target.the('Context menu no show button').located(by.xpathSingleElement('//span[contains(text(),"No Show")]/parent::*')),

    // Patient options
    Context_menu_patient_button: Target.the('Context menu patient button').located(by.xpathSingleElement('//div[@id="scheduler_aptMenuBlock_SMAPT"]//span[text()="Patient"]/parent::*')),
    Context_menu_go_to_patient_button: Target.the('Context menu go to patient button').located(by.xpathSingleElement('//span[contains(text(),"Go To Patient")]')),
    Context_menu_read_notes_button: Target.the('Context menu read notes button').located(by.xpathSingleElement('//span[text()="Read Notes"]/parent::*')),
    Context_menu_write_chart_note_button: Target.the('Context menu write chart note button').located(by.xpathSingleElement('//span[text()="Write Chart Note"]/parent::*')),
    Context_menu_write_internal_note_button: Target.the('Context menu write internal note button').located(by.xpathSingleElement('//span[text()="Write Internal Note"]/parent::*')),
    
    // Existing patient modal elements
    Existing_patient_name_field: Target.the('Existing patient name field').located(by.id('patientId_I')),
    Existing_patient_type_field: Target.the('Existing patient type field').located(by.id('appttypes_B-1')),
    Existing_patient_comments_field: Target.the('Existing patient Comments field').located(by.id('apptdetail')),
    Save_existing_patient_button: Target.the('Save existing patient button').located(by.id('saveapptpatientbtn')),
    Cancel_existing_patient_button: Target.the('Cancel existing patient button').located(by.id('exitapptcancelbtn')),

    // New patient modal elements
    New_patient_name_field: Target.the('New patient name field').located(by.id('newptfname')),
    New_patient_MI_field: Target.the('New patient MI field').located(by.xpath('//input[@name="MI"]')),
    New_patient_last_name_field: Target.the('New patient last name field').located(by.id('newptlname')),
    New_patient_next_button: Target.the('New patient next button').located(by.id('newptnext')),
    New_patient_already_seen_button: Target.the('New patient already seen button').located(by.xpath('//input[@id="btnA"]/parent::*')),
    New_patient_not_yet_seen_button: Target.the('New patient not yet seen button').located(by.xpath('//input[@id="btnN"]/parent::*')),
    New_patient_dob_field: Target.the('New patient DoB field').located(by.id('DOB_I')),
    New_patient_female_button: Target.the('New patient female button').located(by.xpath('//input[@id="btnF"]/parent::*')),
    New_patient_male_button: Target.the('New patient male button').located(by.xpath('//input[@id="btnM"]/parent::*')),
    
    // New patient address elements
    New_patient_manual_entry_button: Target.the('New patient manual address entry button').located(by.id('showmanuallocation')),
    New_patient_address_field: Target.the('New patient  address field').located(by.id('fulladdress')),
    New_patient_city_field: Target.the('New patient city field').located(by.id('locality')),
    New_patient_postcode_field: Target.the('New patient postcode field').located(by.id('postal_code')),
    New_patient_country_field: Target.the('New patient country field').located(by.id('country')),
    
    // Personal details
    New_patient_phone_field: Target.the('New patient phone field').located(by.id('phoneprimary')),
    New_patient_mobile_phone_checkbox: Target.the('New patient mobile phone checkbox').located(by.id('cbIsMobile_S_D')),
    New_patient_text_reminder_checkbox: Target.the('New patient text reminder checkbox').located(by.id('cbReminder_S_D')),
    New_patient_message_checkbox: Target.the('New patient message checkbox').located(by.id('cbMessage_S_D')),
    New_patient_email_field: Target.the('New patient email field').located(by.id('email')),

    // Referred section
    New_patient_unknown_refer_button: Target.the('New patient unknown referrer button').located(by.xpath('//input[@name="referral"][@value="1"]/parent::*')),
    New_patient_friend_refer_button: Target.the('New patient friend referrer button').located(by.xpath('//input[@name="referral"][@value="6"]/parent::*')),
    New_patient_patient_refer_button: Target.the('New patient patient referrer button').located(by.xpath('//input[@name="referral"][@value="2"]/parent::*')),
    New_patient_staff_refer_button: Target.the('New patient staff referrer button').located(by.xpath('//input[@name="referral"][@value="3"]/parent::*')),
    New_patient_doctor_refer_button: Target.the('New patient doctor referrer button').located(by.xpath('//input[@name="referral"][@value="4"]/parent::*')),
    New_patient_marketing_refer_button: Target.the('New patient marketing referrer button').located(by.xpath('//input[@name="referral"][@value="5"]/parent::*')),
    New_patient_custom1_field: Target.the('New patient custom1 field').located(by.id('Custom1')),
    New_patient_custom2_field: Target.the('New patient custom2 field').located(by.id('Custom2')),
    New_patient_custom3_field: Target.the('New patient custom3 field').located(by.id('Custom3')),
    New_patient_primary_care_field: Target.the('New patient primary care provider field').located(by.id('primarycaredr_I')),
    New_patient_save_button: Target.the('New patient save button').located(by.id('savenewpatientbtn')),
    New_patient_cancel_button: Target.the('New patient primary care provider field').located(by.xpath('//button[@onclick="scheduler.AppointmentFormCancel();"]')),
    New_patient_save_and_email_button: Target.the('New patient save and email button').located(by.xpath('savenewpatientportalbtn')),

    // New patient appointment elements
    New_patient_appointment_concern_field: Target.the('New patient concern field').located(by.id('consultReasonCbo_I')),
    New_patient_appointment_comments_field: Target.the('New patient comments field').located(by.id('apptdetail')),
    New_patient_appointment_save_button: Target.the('New patient save button').located(by.id('newapptpatientbtn')),
    New_patient_appointment_cancel_button: Target.the('New patient cancel button').located(by.id('newapptcancelbtn')),
    Provider_text_field: Target.the('Provider dropdown').located(by.id('simpleprovidercbo_I')),
    Provider_dropdown_button: Target.the('Provider dropdown').located(by.id('simpleprovidercbo_B-1')),

    // Existing patient appointment elements
    Search_patient_field: Target.the('Search patient field').located(by.id('patientId_I')),
    Appointment_type_dropdown: Target.the('Appointment type dropdown').located(by.id('appttypes_I')),
    Save_appointment_button: Target.the('Save appointment button').located(by.id('saveapptpatientbtn')),




    //General
    Scheduler_Loader: Target.the('Scheduler loader').located(by.id('scheduler_LPV')),
    Appointment_summary_text: Target.the('appointment Summary text').located(by.xpath('//span[contains(text(), "Appointment Summary")]')),

    //l oader
    Patient_Loader: Target.the('Scheduler loader').located(by.id('patientId_LPV')),

    


};
