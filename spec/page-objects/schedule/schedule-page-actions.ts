import { Scroll, See, Task, Text, Wait, Is, Enter, Duration, Click, Target, Interaction, BrowseTheWeb, Clear } from 'serenity-js/lib/screenplay-protractor';

import { schedulePage } from './ui/schedule-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { generalElements } from '../general/ui/general-elements';
import { by, element, protractor, browser } from 'protractor';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);

// Actions for the Happening modal dialog
export const scheduleHappeningModalActions = {

    /**
    * Enters the provided string in the happening subject field
    */
    enterHappeningSubject: (text: string) => Task.where(`{0} enters subject text as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.Happening_Subject_field, Is.visible()),
        Scroll.to(schedulePage.Happening_Subject_field),
        Enter.theValue(text).into(schedulePage.Happening_Subject_field)
    ),
    /**
    * Enters the provided string in the happening comments field
    */
    enterHappeningComments: (text: string) => Task.where(`{0} enters comments text as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.Happening_Comments_field, Is.visible()),
        Scroll.to(schedulePage.Happening_Comments_field),
        Enter.theValue(text).into(schedulePage.Happening_Comments_field)
    ),

    /**
    * Clicks on the All day happening checkbox
    */
    clickAllDayCheckbox: () => Task.where(`{0} clicks the All Day checkbox`,
        Wait.upTo(waitTime).until(schedulePage.Happening_All_day_checkbox, Is.clickable()),
        Scroll.to(schedulePage.Happening_All_day_checkbox),
        Click.on(schedulePage.Happening_All_day_checkbox),
    ),

    /**
    * Clicks on the Recurring checkbox
    */
    clickRecurringCheckbox: () => Task.where(`{0} clicks Recurring checkbox`,
        Wait.upTo(waitTime).until(schedulePage.Happening_Recurrence_checkbox, Is.clickable()),
        Scroll.to(schedulePage.Happening_Recurrence_checkbox),
        Click.on(schedulePage.Happening_Recurrence_checkbox),
    ),

    /**
    * Clicks on save happenning button
    * The subject and type must be previously entered for the button to be clickable
    */
    clickSaveHappeningButton: () => Task.where(`{0} clicks save happenning button`,
        Wait.upTo(waitTime).until(schedulePage.Save_happening_button, Is.clickable()),
        Scroll.to(schedulePage.Save_happening_button),
        Click.on(schedulePage.Save_happening_button),
    ),

    /**
    * Clicks on the Cancel happening button
    */
    clickCancelHappeningButton: () => Task.where(`{0} clicks cancel happenning button`,
        Wait.upTo(waitTime).until(schedulePage.Cancel_happening_button, Is.clickable()),
        Scroll.to(schedulePage.Cancel_happening_button),
        Click.on(schedulePage.Cancel_happening_button),
    ),

    expandAppointmentTypeDropdown: () => Task.where(`{0} clicks appointment type dropdown`,
        Wait.upTo(waitTime).until(schedulePage.Appointment_type_dropdown, Is.clickable()),
        Scroll.to(schedulePage.Appointment_type_dropdown),
        Click.on(schedulePage.Appointment_type_dropdown),
    ),


    selectHappeningType: () => Task.where(`{0} selects the happening type`,
        Wait.upTo(waitTime).until(schedulePage.Happening_type_option, Is.clickable()),
        Scroll.to(schedulePage.Happening_type_option),
        Click.on(schedulePage.Happening_type_option),
    ),

    enterTextInAppointmentTypeField: (text: string) => Task.where(`{0} enters ${text} in the appointment type field`,
        Wait.upTo(waitTime).until(schedulePage.Happening_type_field, Is.clickable()),
        Scroll.to(schedulePage.Happening_type_field),
        Enter.theValue(text).into(schedulePage.Happening_type_field),
        Wait.for(Duration.ofMillis(3000))
    ),

    /**
    * Takes exact text string as argument and creates a locator
    * Selects the provided value from the dropdown
    */
    selectDurationOfHappening: (duration: string) => {
        let elem = Target.the(`Duration as ${duration}`).located(by.xpath(`//select[@id="apptduration"]//option[text()="${duration}"]`))
        return Task.where(`{0} selects the ${duration} in the duration dropdown`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },
}


export const scheduleContextMenuOptions = {
    /**
    * Clicks the New Patient button
    * The context menu needs to be previously clicked in order for the element to be visible
    */
    clickTheNewPatientButton: () => Task.where(`{0} clicks the New Patient button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.New_patient_button, Is.clickable()),
        Scroll.to(schedulePage.New_patient_button),
        Click.on(schedulePage.New_patient_button),
    ),
    /**
    * Clicks the Existing Patient button
    * The context menu needs to be previously clicked in order for the element to be visible
    */
    clickTheExistingPatientButton: () => Task.where(`{0} clicks the Existing Patient button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Existing_patient_button, Is.clickable()),
        Scroll.to(schedulePage.Existing_patient_button),
        Click.on(schedulePage.Existing_patient_button),
    ),
    /**
    * Clicks the Happening button
    * The context menu needs to be previously clicked in order for the element to be visible
    */
    clickTheHappeningButton: () => Task.where(`{0} clicks the Happening button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Happening_button, Is.clickable()),
        Scroll.to(schedulePage.Happening_button),
        Click.on(schedulePage.Happening_button),
    ),

    /**
    * Clicks the Paste button
    * The context menu needs to be previously clicked in order for the element to be visible
    */
    clickThePasteButton: () => Task.where(`{0} clicks the Paste button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Paste_button, Is.clickable()),
        Scroll.to(schedulePage.Paste_button),
        Click.on(schedulePage.Paste_button),
    ),
}


// Actions related to the Schedule page in general
export const schedulePageActions = {
    /**
    * Takes a string as input and creates a locator based on the string
    * When testing a happy path the subject line of the happening must be the same as the one provided
    * @example
    * scheduleHappeningModalActions.enterHappeningSubject("Example subject");
    * scheduleHappeningModalActions.clickSaveHappeningButton();
    * schedulePageActions.checkIfHappeningWasCreated("Example subject"); // this will be marked as passed
    */
    checkIfHappeningWasCreated: (name: string) => {
        let elem: Target = Target.the(`Happening with name ${name}`).located(by.xpath(`//span[@class="dxeBase_Moderno"][text()="${name}"]`));
        return Task.where(`{0} checks if the ${name} happening is visible in schedule`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
        )
    },



    checkIfNewPatientAppointmentWasCreated: (name: string) => {
        let elem: Target = Target.the(`Appointment with name ${name}`).located(by.xpath(`//span[contains(text(),"${name}")]`));
        return Task.where(`{0} checks if the ${name} happening is visible in schedule`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
        )
    },
    checkIfAppointmentIsAbsent: (name: string) => {
        let elem: Target = Target.the(`Appointment with name ${name}`).located(by.xpath(`//span[contains(text(),"${name}")]`));
        return Task.where(`{0} checks if the ${name} happening is visible in schedule`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.absent()),
        )
    },

    clickTheStretchShrinkButton: () => Task.where(`{0} clicks the stretch/shrink button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Stretch_Shrink_Button, Is.clickable()),
        Scroll.to(schedulePage.Stretch_Shrink_Button),
        Click.on(schedulePage.Stretch_Shrink_Button),
    ),


    clickPrintScheduleButton: () => Task.where(`{0} clicks the print schedule button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Print_Schedule_Button, Is.clickable()),
        Scroll.to(schedulePage.Print_Schedule_Button),
        Click.on(schedulePage.Print_Schedule_Button),
    ),

    clickMoveUpListButton: () => Task.where(`{0} clicks the move up list button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Move_Up_List_Button, Is.clickable()),
        Scroll.to(schedulePage.Move_Up_List_Button),
        Click.on(schedulePage.Move_Up_List_Button),
    ),

    clickRefreshScheduleButton: () => Task.where(`{0} clicks the refresh schedule button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Refresh_Schedule_Button, Is.clickable()),
        Scroll.to(schedulePage.Refresh_Schedule_Button),
        Click.on(schedulePage.Refresh_Schedule_Button),
    ),

    clickShowInactiveButton: () => Task.where(`{0} clicks the show inactive button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Show_Inactive_Button, Is.clickable()),
        Scroll.to(schedulePage.Show_Inactive_Button),
        Click.on(schedulePage.Show_Inactive_Button),
    ),

    enterTextInSearchField: (text: string) => Task.where(`{0} enters search text as ${text}`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Show_Inactive_Button, Is.visible()),
        Scroll.to(schedulePage.Show_Inactive_Button),
        Enter.theValue(text).into(schedulePage.Search_Appointments_Field)
    ),

    clickSearchAppointmentsButton: () => Task.where(`{0} clicks the search appointments button`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Search_Appointments_Button, Is.clickable()),
        Scroll.to(schedulePage.Search_Appointments_Button),
        Click.on(schedulePage.Search_Appointments_Button),
    ),

    clickLeftArrowSidebarCalendar: () => Task.where(`{0} clicks the left arrow in the sidebar calendar`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.Search_Appointments_Button, Is.clickable()),
        Scroll.to(schedulePage.Search_Appointments_Button),
        Click.on(schedulePage.Search_Appointments_Button),
    ),

    // This takes the desired day as a number and then uses it to construct a locator for that exact day
    clickDateInSidebarCalendar: (dayNumber: number) => {
        let elem = Target.the(`${dayNumber} day from the sidebar calendar`).located(by.xpath(`//table[@id="dateNavigatorScheduler_cal_mt"]//td[text()="${dayNumber}"]`))
        return Task.where(`{0} clicks the ${dayNumber} in the sidebar calendar`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    /**
    * Takes an exact string as parameter and creates a locator in the jump ahead section
    */
    clickJumpAheadWeeks: (weeks: string) => {
        let elem = Target.the(`jump ahead ${weeks} from the jump ahead section`).located(by.xpath(`//div[@id="sidebar-wrapper"]//button[contains(text(),"${weeks}")]`))
        return Task.where(`{0} clicks the ${weeks} in the jump ahead section`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    /**
    * Takes a string with partial text and creates a locator in the filter schedule section
    */
    filterSchedule: (partialText: string) => {
        let elem = Target.the(`Schedule filter for ${partialText} from the Schedules section`).located(by.xpath(`//table[@id="cbResources"]//label[contains(text(),"${partialText}")]`))
        return Task.where(`{0} clicks the ${partialText} in the jump ahead section`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickEditButtonContextMenu: () => Task.where(`{0} clicks the edit button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_edit_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_edit_button),
        Click.on(schedulePage.Context_menu_edit_button),
    ),

    clickCopyButtonContextMenu: () => Task.where(`{0} clicks the copy button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_copy_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_copy_button),
        Click.on(schedulePage.Context_menu_copy_button),
    ),
    clickRescheduleButton: () => Task.where(`{0} clicks the reschedule button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_reschedule_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_reschedule_button),
        Click.on(schedulePage.Context_menu_reschedule_button),
    ),
    clickRescheduleByOffice: () => Task.where(`{0} clicks the reschedule button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_reschedule_by_office_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_reschedule_by_office_button),
        Click.on(schedulePage.Context_menu_reschedule_by_office_button),
    ),
    rescheduleByPatient: () => Task.where(`{0} clicks the reschedule button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_reschedule_by_patient_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_reschedule_by_patient_button),
        Click.on(schedulePage.Context_menu_reschedule_by_patient_button),
    ),
    clickPasteButtonContextMenu: () => Task.where(`{0} clicks the paste button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_paste_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_paste_button),
        Click.on(schedulePage.Context_menu_paste_button),
    ),



    clickGoToMoneyButtonContextMenu: () => Task.where(`{0} clicks the go to money button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_go_to_money_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_go_to_money_button),
        Click.on(schedulePage.Context_menu_go_to_money_button),
    ),


    clickTheHistoryButtonContextMenu: () => Task.where(`{0} clicks the history button from the appointment context menu`,
        Wait.upTo(waitTime).until(schedulePage.Context_menu_history_button, Is.clickable()),
        Scroll.to(schedulePage.Context_menu_history_button),
        Click.on(schedulePage.Context_menu_history_button),
    ),


    checkIfAppointmentSummaryOpened: () => Task.where(`{0} checks if the appointment summary opened`,
        Wait.upTo(waitTime).until(schedulePage.Appointment_summary_text, Is.visible()),
    ),
    /**
    * Takes a string as input and clicks a button from the side menu
    * available options listed below
    * @example
    * schedulePageActions.clickButtonFromContextMenuExpanded("reschedule by office"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("reschedule by patient"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("cancel by office"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("cancel by patient"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("go to patient"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("read notes"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("write chart note"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("write internal note"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status arrived"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status in room"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status seen"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status completed"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status left unseen"); 
    * schedulePageActions.clickButtonFromContextMenuExpanded("status no show"); 
    */

    clickButtonFromContextMenuExpanded: (buttonName: string) => {
        let elem: Target;
        buttonName = buttonName.toLowerCase();

        switch (buttonName) {
            case "reschedule by office":
                elem = schedulePage.Context_menu_reschedule_by_office_button;
                break;

            case "reschedule by patient":
                elem = schedulePage.Context_menu_reschedule_by_patient_button;
                break;


            case "cancel by office":
                elem = schedulePage.Context_menu_cancel_by_office_button;
                break;

            case "cancel by patient":
                elem = schedulePage.Context_menu_cancel_by_patient_button;
                break;

            case "go to patient":
                elem = schedulePage.Context_menu_patient_button;
                break;

            case "read notes":
                elem = schedulePage.Context_menu_status_button;
                break;

            case "write chart note":
                elem = schedulePage.Context_menu_write_chart_note_button;
                break;

            case "write internal note":
                elem = schedulePage.Context_menu_write_internal_note_button;
                break;

            case "status arrived":
                elem = schedulePage.Context_menu_status_arrived_button;
                break;

            case "status in room":
                elem = schedulePage.Context_menu_status_in_room_button;
                break;

            case "status seen":
                elem = schedulePage.Context_menu_status_seen_button;
                break;

            case "status completed":
                elem = schedulePage.Context_menu_status_completed;
                break;

            case "status left unseen":
                elem = schedulePage.Context_menu_status_left_unseen_button;
                break;

            case "status no show":
                elem = schedulePage.Context_menu_status_no_show_button;
                break;
        }
        return Task.where(`{0} clicks the ${buttonName} from the appointment sub menu`,
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Click.on(elem)
        )
    },

    /**
    * Takes a string as input and moves to the desired button with the mouse
    * @example
    * schedulePageActions.hoverOverContextMenuButton("patient"); // This will hover over the patient button from the context menu
    */
    hoverOverContextMenuButton: (buttonName: string) => {
        let elem: Target;
        buttonName = buttonName.toLowerCase();
        switch (buttonName) {
            case "reschedule":
                elem = schedulePage.Context_menu_reschedule_button;
                break;

            case "cancel":
                elem = schedulePage.Context_menu_cancel_button;
                break;


            case "patient":
                elem = schedulePage.Context_menu_patient_button;
                break;

            case "status":
                elem = schedulePage.Context_menu_status_button;
                break;
        }
        return Task.where(`{0} hovers over the ${buttonName} from the appointment context menu`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(
                `hovers over element`,
                async actor => {
                    //let elemLoc = await BrowseTheWeb.as(actor).locate(elem).getLocation();
                    await browser.actions()
                        .mouseMove(BrowseTheWeb.as(actor).locate(elem))
                        .perform()
                })
        )
    },

    /**
    * Takes a string as input and creates a locator based on the string
    * The locator is then used to find the desired time in the calendar and right click on that location
    * The string must be a number folowed by am or pm
    * @example
    * schedulePageActions.rightClickOnCalendar("7am"); // This will right click on the 7am field
    * schedulePageActions.rightClickOnCalendar("7pm"); // This will right click on the 7pm field
    */
    rightClickOnCalendar: (calendarTime: string) => {
        // Removing am or pm from the string for the locator
        let newCalendarTime = calendarTime.replace(/am|pm/g, "");

        return Task.where(`{0} clicks the ${calendarTime} in the jump ahead section`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(generalElements.Schedule_Button, Is.clickable()),
            Interaction.where(
                `right clicks on location`,
                async actor => {
                    let elem;
                    if (calendarTime.includes("am"))
                        elem = element.all(by.xpath(`//table[@id="scheduler_containerBlock_vertTable"]//td[text()="${newCalendarTime}"]/parent::*//td[@class="dxscTimeCellBody_Moderno dxsc-time-cell-body"]`)).first();
                    else
                        elem = element.all(by.xpath(`//table[@id="scheduler_containerBlock_vertTable"]//td[text()="${newCalendarTime}"]/parent::*//td[@class="dxscTimeCellBody_Moderno dxsc-time-cell-body"]`)).last();
                    await browser.actions().mouseMove(elem).perform();
                    await browser.actions().click(protractor.Button.RIGHT).perform();
                })
        )
    },

    rightClickOnAppointment: (apptName: string) => {
        let elem: Target = Target.the(`${apptName} appointment`).located(by.xpathSingleElement(`//span[contains(text(),"${apptName}")]`))
        return Task.where(`{0} right clicks the ${apptName} in schedule`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(
                `right clicks on element`,
                async actor => {
                    await browser.actions().mouseMove(BrowseTheWeb.as(actor).locate(elem)).perform();
                    await browser.actions().click(protractor.Button.RIGHT).perform();
                })
        )
    },

    clickOnAppointment: (apptName: string) => {
        let elem: Target = Target.the(`${apptName} appointment`).located(by.xpathSingleElement(`//span[contains(text(),"${apptName}")]`))
        return Task.where(`{0} right clicks the ${apptName} in schedule`,
            checkIfScheduleLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Click.on(elem)
        )
    },
}

/**
* This is a general function that checks if any loaders or modals are present on the page
* By calling this function before performing any action, errors like "Other element would receive the click" are avoided
*/
export const waitForAllTheModalsToBeAbsent = () => Task.where(`{0} check if there are any modals and wait till they disappear`,
    Interaction.where(
        `#checks any modals or loaders are present`,
        async actor => {
            // Current list of known modals that might appear
            var quoteLoader = element(by.xpath('//div[@id="QuoteProcedureGrid_LD"]'));
            var EC = protractor.ExpectedConditions;
            let vueLoader = element(by.xpath('//div[@id="vueModal"]/div/div'));
            let loader = element(by.xpath('scheduler_LPV'));
            let generalLoader = element(by.xpath('//div[@style="padding-top: 15%; overflow-y: visible; display: block; padding-left: 0px;"]'));
            let modalBackdrop = element(by.xpath('//div[@class="modal-backdrop fade"]'));
            let dxModalSys = element(by.xpath('//div[@class="dxmodalSys"]'));
            let modalFade = element(by.xpath('//div[@class="modal-dialog modal-m"]'));
            let modalContent = element.all(by.xpath('//div[@class="modal-content"]'));
            let cartLoader = element(by.id('CartRegisterGrid_LPV'));






            // This is an array of modals that the patients page has
            let modals = element.all(by.xpath('//div[@class="modal-dialog"]'));
            modals.each(async modal => {
                await browser.wait(EC.not(EC.visibilityOf(modal)), 30000, "Modal dialog didn't disappear").catch(err => {
                    throw err;
                })
            })
            modalContent.each(async modal => {
                await browser.wait(EC.not(EC.visibilityOf(modal)), 30000, "Modal dialog didn't disappear").catch(err => {
                    throw err;
                })
            })
            // Checking for visibility of modals
            await browser.wait(EC.not(EC.visibilityOf(vueLoader)), 30000, "Vue Loader didn't disappear").catch(err => {
                throw err;
            })

            await browser.wait(EC.not(EC.visibilityOf(cartLoader)), 30000, "Cart Loader didn't disappear").catch(err => {
                throw err;
            })
            await browser.wait(EC.not(EC.visibilityOf(loader)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })

            await browser.wait(EC.not(EC.visibilityOf(generalLoader)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })

            await browser.wait(EC.not(EC.visibilityOf(quoteLoader)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })
            await browser.wait(EC.not(EC.visibilityOf(modalBackdrop)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })

            await browser.wait(EC.not(EC.visibilityOf(dxModalSys)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })

            await browser.wait(EC.not(EC.visibilityOf(modalFade)), 30000, "Loader didn't disappear").catch(err => {
                throw err;
            })

        })
)


export const existingPatientAppointmentActions = {

    enterPatientNameInSearchBar: (text: string) => Task.where(`{0} enters patient name as ${text}`,
        checkIfScheduleLoaderAppears(),
        Wait.upTo(waitTime).until(schedulePage.Search_patient_field, Is.visible()),
        Scroll.to(schedulePage.Search_patient_field),
        Enter.theValue(text).into(schedulePage.Search_patient_field)
    ),


    clickOnAppointmentTypeDropdown: () => Task.where(`{0} clicks on the appointment dropdown`,
        checkIfPatientLoaderAppears(),
        waitForLoaderToDisappear(),
        Wait.upTo(waitTime).until(schedulePage.Appointment_type_dropdown, Is.visible()),
        Scroll.to(schedulePage.Appointment_type_dropdown),
        Click.on(schedulePage.Appointment_type_dropdown)
    ),

    selectAppointmentType: (text: string) => {
        //text = text.toLowerCase();
        let elem = Target.the(`${text} appointment type`).located(by.xpath(`//td[contains(text(),"${text}")]`))
        return Task.where(`{0} selects the appointment type as ${text}`,
            checkIfScheduleLoaderAppears(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },

    clickSaveAppointmentButton: () => Task.where(`{0} clicks on the appointment dropdown`,
        checkIfScheduleLoaderAppears(),
        Wait.upTo(waitTime).until(schedulePage.Save_appointment_button, Is.visible()),
        Scroll.to(schedulePage.Save_appointment_button),
        Click.on(schedulePage.Save_appointment_button)
    ),
};

export const newPatientAppointmentActions = {
    enterPatientName: (text: string) => Task.where(`{0} enters patient name as ${text}`,
        checkIfScheduleLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(schedulePage.New_patient_name_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_name_field),
        Clear.theValueOf(schedulePage.New_patient_name_field),
        Enter.theValue(text).into(schedulePage.New_patient_name_field)
    ),

    enterPatientMi: (text: string) => Task.where(`{0} enters patient MI as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_MI_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_MI_field),
        Clear.theValueOf(schedulePage.New_patient_MI_field),
        Enter.theValue(text).into(schedulePage.New_patient_MI_field)
    ),

    enterPatientSurname: (text: string) => Task.where(`{0} enters patient surname as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_last_name_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_last_name_field),
        Clear.theValueOf(schedulePage.New_patient_last_name_field),
        Enter.theValue(text).into(schedulePage.New_patient_last_name_field)
    ),

    clickNextButton: () =>
        Task.where(`{0} clicks the next button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_next_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_next_button),
            Click.on(schedulePage.New_patient_next_button),
        ),

    clickAlreadySeenButton: () =>
        Task.where(`{0} clicks the already seen button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_already_seen_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_already_seen_button),
            Click.on(schedulePage.New_patient_already_seen_button),
        ),

    clickNotYetSeenButton: () =>
        Task.where(`{0} clicks the not yet seen button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_not_yet_seen_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_not_yet_seen_button),
            Click.on(schedulePage.New_patient_not_yet_seen_button),
        ),

    expandProviderDropdown: () =>
        Task.where(`{0} expand provider dropdown`,
            Wait.upTo(waitTime).until(schedulePage.Provider_dropdown_button, Is.clickable()),
            Scroll.to(schedulePage.Provider_dropdown_button),
            Click.on(schedulePage.Provider_dropdown_button),
        ),


    selectProvider: () => {
        let elem = Target.the(`Selects the provider`).located(by.id(`simpleprovidercbo_DDD_L_LBI1T0`))
        return Task.where(`{0} selects theprovider`,
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    enterPatientDob: (text: string) => Task.where(`{0} enters patient DoB as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_dob_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_dob_field),
        Clear.theValueOf(schedulePage.New_patient_dob_field),
        Enter.theValue(text).into(schedulePage.New_patient_dob_field)
    ),

    clickMaleGenderButton: () =>
        Task.where(`{0} clicks male gender button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_male_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_male_button),
            Click.on(schedulePage.New_patient_male_button),
        ),

    clickFemaleGenderButton: () =>
        Task.where(`{0} clicks female gender button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_female_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_female_button),
            Click.on(schedulePage.New_patient_female_button),
        ),

    clickManualEntryAddressButton: () =>
        Task.where(`{0} clicks manual entry address button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_manual_entry_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_manual_entry_button),
            Click.on(schedulePage.New_patient_manual_entry_button),
        ),

    selectState: (state: string) => {
        let elem: Target = Target.the(`Option ${state}`).located(by.xpath(`//option[@value="${state}"]`))
        return Task.where(`{0} clicks the ${state} option`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    enterAddress: (text: string) => Task.where(`{0} enters patient address as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_address_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_address_field),
        Clear.theValueOf(schedulePage.New_patient_address_field),
        Enter.theValue(text).into(schedulePage.New_patient_address_field)
    ),

    enterProviderName: (text: string) => Task.where(`{0} enters provider name as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.Provider_text_field, Is.visible()),
        Scroll.to(schedulePage.Provider_text_field),
        Clear.theValueOf(schedulePage.Provider_text_field),
        Enter.theValue(text).into(schedulePage.Provider_text_field)
    ),

    enterCity: (text: string) => Task.where(`{0} enters patient city as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_city_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_city_field),
        Clear.theValueOf(schedulePage.New_patient_city_field),
        Enter.theValue(text).into(schedulePage.New_patient_city_field)
    ),

    enterPostCode: (text: string) => Task.where(`{0} enters patient postcode as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_postcode_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_postcode_field),
        Enter.theValue(text).into(schedulePage.New_patient_postcode_field),
    ),

    enterCountryCode: (text: string) => Task.where(`{0} enters patient countryCode as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_country_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_country_field),
        Clear.theValueOf(schedulePage.New_patient_country_field),
        Enter.theValue(text).into(schedulePage.New_patient_country_field),
    ),

    enterPhone: (text: string) => Task.where(`{0} enters patient phone as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_phone_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_phone_field),
        Clear.theValueOf(schedulePage.New_patient_phone_field),
        Enter.theValue(text).into(schedulePage.New_patient_phone_field),
    ),

    clickMobilePhoneCheckbox: () =>
        Task.where(`{0} clicks mobile phone checkbox`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_mobile_phone_checkbox, Is.clickable()),
            Scroll.to(schedulePage.New_patient_mobile_phone_checkbox),
            Clear.theValueOf(schedulePage.New_patient_name_field),
            Click.on(schedulePage.New_patient_mobile_phone_checkbox),
        ),

    clickTextReminderCheckbox: () =>
        Task.where(`{0} clicks the text reminder checkbox`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_text_reminder_checkbox, Is.clickable()),
            Scroll.to(schedulePage.New_patient_text_reminder_checkbox),
            Click.on(schedulePage.New_patient_text_reminder_checkbox),
        ),

    clickMessageChecbox: () =>
        Task.where(`{0} clicks the messge checkbox`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_message_checkbox, Is.clickable()),
            Scroll.to(schedulePage.New_patient_message_checkbox),
            Click.on(schedulePage.New_patient_message_checkbox),
        ),

    enterEmail: (text: string) => Task.where(`{0} enters patient email as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_email_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_email_field),
        Clear.theValueOf(schedulePage.New_patient_email_field),
        Enter.theValue(text).into(schedulePage.New_patient_email_field),
    ),

    selectReferrer: (referrer: string) => {
        referrer = referrer.toLowerCase();
        let elem: Target;
        switch (referrer) {
            case "unknown":
                elem = schedulePage.New_patient_unknown_refer_button;
                break;

            case "friend":
                elem = schedulePage.New_patient_friend_refer_button;
                break;


            case "patient":
                elem = schedulePage.New_patient_patient_refer_button;
                break;


            case "staff":
                elem = schedulePage.New_patient_staff_refer_button;
                break;


            case "doctor":
                elem = schedulePage.New_patient_doctor_refer_button;
                break;


            case "marketing":
                elem = schedulePage.New_patient_marketing_refer_button;
                break;
        }
        return Task.where(`{0} clicks the ${referrer} button`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    enterTextInCustom1: (text: string) => Task.where(`{0} enters patient custom1 as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_custom1_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_custom1_field),
        Clear.theValueOf(schedulePage.New_patient_custom1_field),
        Enter.theValue(text).into(schedulePage.New_patient_custom1_field),
    ),


    enterTextInCustom2: (text: string) => Task.where(`{0} enters patient custom2 as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_custom2_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_custom2_field),
        Clear.theValueOf(schedulePage.New_patient_custom2_field),
        Enter.theValue(text).into(schedulePage.New_patient_custom2_field)
    ),


    enterTextInCustom3: (text: string) => Task.where(`{0} enters patient custom3 as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_custom3_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_custom3_field),
        Clear.theValueOf(schedulePage.New_patient_custom3_field),
        Enter.theValue(text).into(schedulePage.New_patient_custom3_field)
    ),

    enterConcern: (text: string) => Task.where(`{0} enters patient concern as ${text}`,
        Wait.upTo(waitTime).until(schedulePage.New_patient_appointment_concern_field, Is.visible()),
        Scroll.to(schedulePage.New_patient_appointment_concern_field),
        Clear.theValueOf(schedulePage.New_patient_appointment_concern_field),
        Enter.theValue(text).into(schedulePage.New_patient_appointment_concern_field)
    ),

    clickSaveButton: () =>
        Task.where(`{0} clicks save new patient button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_save_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_save_button),
            Click.on(schedulePage.New_patient_save_button),
        ),

    clickNewPatientAppointmentSaveButton: () =>
        Task.where(`{0} clicks save new patient appointment button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_appointment_save_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_appointment_save_button),
            Wait.for(Duration.ofMillis(2000)),
            Click.on(schedulePage.New_patient_appointment_save_button),
        ),

    clickNewPatientAppointmentCanceButton: () =>
        Task.where(`{0} clicks cacel new patient appointment button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_appointment_cancel_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_appointment_cancel_button),
            Click.on(schedulePage.New_patient_appointment_cancel_button),
        ),

    clickCancelButton: () =>
        Task.where(`{0} clicks the cancel new patient button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_cancel_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_cancel_button),
            Click.on(schedulePage.New_patient_cancel_button),
        ),

    clickEmailAndSaveButton: () =>
        Task.where(`{0} clicks save new patient button`,
            Wait.upTo(waitTime).until(schedulePage.New_patient_save_and_email_button, Is.clickable()),
            Scroll.to(schedulePage.New_patient_save_and_email_button),
            Click.on(schedulePage.New_patient_save_and_email_button),
        ),
}

// This function waits for 1.5s if a loader appears
// This was implemented due to the fact that sometimes the loader doesn't appear right away and clicking on something failed
// TODO implement a better workaround for detection instead of hard wait
export const checkIfScheduleLoaderAppears = () =>
    Interaction.where(
        `#checks if the loader has appeared`,
        async actor => {
            let loader = element(by.xpath('scheduler_LPV'));
            var until = protractor.ExpectedConditions;
            await browser.wait(until.presenceOf(loader), 1500, "Didn't appear").catch(err => {
                return;
            })
        })


export const checkIfPatientLoaderAppears = () =>
    Interaction.where(
        `#checks if the patient loader has appeared`,
        async actor => {
            let loader = element(by.xpath('patientId_LPV'));
            var until = protractor.ExpectedConditions;
            await browser.wait(until.presenceOf(loader), 1500, "Didn't appear").catch(err => {
                return;
            })
        })

export const waitForLoaderToDisappear = () => Task.where(`{0} cwaits for the loader to disappear`,
    checkIfPatientLoaderAppears(),
    Wait.upTo(waitTime).until(schedulePage.Patient_Loader, Is.absent()),
)

// assertions
const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
