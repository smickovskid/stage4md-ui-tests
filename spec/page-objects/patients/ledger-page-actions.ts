import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target, BrowseTheWeb, Actor, Clear } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { checkIfLoaderAppears } from '../general';
import { summaryPage } from './ui/summary-page';
import { profilePage } from './ui/profile-page';
import { patientsPage } from './ui/patients-page';
import { ledger } from './ui/ledger-page';
import { by } from 'protractor';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);
let actor = Actor.named("james").whoCan(BrowseTheWeb);
export const ledgerPageActions = {

    clickIssueCreditRefundButton: () => Task.where(`{0} clicks the issue credit/refund button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(ledger.Issue_credit_refund_button, Is.clickable()),
        Scroll.to(ledger.Issue_credit_refund_button),
        Click.on(ledger.Issue_credit_refund_button),
    ),

    clickProductReturnButton: () => Task.where(`{0} clicks the product return button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(ledger.Product_return_button, Is.clickable()),
        Scroll.to(ledger.Product_return_button),
        Click.on(ledger.Product_return_button),
    ),

    enterAmountInRefundFeeField: (amount: number) => Task.where(`{0} enters ${amount} in the refund fee field`,
        Wait.upTo(waitTime).until(ledger.Refund_fee_field, Is.visible()),
        Scroll.to(ledger.Refund_fee_field),
        Clear.theValueOf(ledger.Refund_fee_field),
        Enter.theValue(amount).into(ledger.Refund_fee_field)
    ),

    enterRefundAmountInField: (amount: number) => Task.where(`{0} enters ${amount} in the refund amount field`,
        Wait.upTo(waitTime).until(ledger.Refund_amount_field, Is.visible()),
        Scroll.to(ledger.Refund_amount_field),
        Clear.theValueOf(ledger.Refund_amount_field),
        Enter.theValue(amount).into(ledger.Refund_amount_field)
    ),

    enterTextInNoteField: (text: string) => Task.where(`{0} enters ${text} in the note field`,
        Wait.upTo(waitTime).until(ledger.Refund_note_field, Is.visible()),
        Scroll.to(ledger.Refund_note_field),
        Clear.theValueOf(ledger.Refund_note_field),
        Enter.theValue(text).into(ledger.Refund_note_field)
    ),

    clickTheRefundNowButton: () => Task.where(`{0} clicks the refund now button`,
        Wait.upTo(waitTime).until(ledger.Refunding_now_button, Is.clickable()),
        Scroll.to(ledger.Refunding_now_button),
        Click.on(ledger.Refunding_now_button),
    ),

    clickAddToListButton: () => Task.where(`{0} clicks the add to list button`,
        Wait.upTo(waitTime).until(ledger.Add_to_list_with_others_button, Is.clickable()),
        Scroll.to(ledger.Add_to_list_with_others_button),
        Click.on(ledger.Add_to_list_with_others_button),
    ),

    clickSaveRefundButton: () => Task.where(`{0} clicks the save refund button`,
        Wait.upTo(waitTime).until(ledger.Save_refund_button, Is.clickable()),
        Scroll.to(ledger.Save_refund_button),
        Click.on(ledger.Save_refund_button),
    ),


    selectRefundReason: (reason: string) => {
        let elem: Target;
        switch (reason) {
            case "PC":
                elem = ledger.Patient_cancelation_refund_option;
                break;
            case "PD":
                elem = ledger.Patient_dissatisifed_refund_option;
                break;
            case "PNP":
                elem = ledger.Procedure_not_performed_refund_option;
                break;

            default:
                elem = ledger.Procedure_not_performed_refund_option;
                break;

        }

        return Task.where(`{0} Selects the ${reason} for refund`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    selectRefundMethod: (method: string) => {
        let elem: Target;
        switch (method) {
            case "CareCredit":
                elem = ledger.CareCredit_method_refund_option;
                break;
            case "Cash":
                elem = ledger.Cash_method_refund_option;
                break;
            case "Check":
                elem = ledger.Check_method_refund_option;
                break;

            case "Credit Card":
                elem = ledger.Credit_card_method_refund_option;
                break;
            default:
                elem = ledger.Credit_card_method_refund_option;
                break;

        }

        return Task.where(`{0} Selects the ${method} for refund`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },


    clickViewReceiptButtonForPatient: (patientName: string) => {
        let elem = Target.the(`Receipt button for ${patientName}`).located(by.xpath(`//td[contains(text(),"${patientName}")]/parent::*//img[@alt="View Receipt"]`))
        return Task.where(`{0} views the recipt for ${patientName}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickProductReturnButtonForPatient: (patientName: string) => {
        let elem = Target.the(`Product return button for ${patientName}`).located(by.xpath(`//td[contains(text(),"${patientName}")]/parent::*//img[@alt="Create Refund"]`))
        return Task.where(`{0} product return button for ${patientName}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    }




}

// assertions

const expect = chai.expect;