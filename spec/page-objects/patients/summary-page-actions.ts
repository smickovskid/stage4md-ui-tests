import { Scroll, Task, Wait, Is, Enter, Duration, Click } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { checkIfLoaderAppears } from '../general';
import { summaryPage } from './ui/summary-page';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);

export const summaryPageActions = {

    clickOnAllergiesButton: () => Task.where(`{0} clicks the allergies button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Allergies_button, Is.clickable()),
        Scroll.to(summaryPage.Allergies_button),
        Click.on(summaryPage.Allergies_button),
    ),

    clickOnAlertsButton: () => Task.where(`{0} clicks the alerts button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Alerts_button, Is.clickable()),
        Scroll.to(summaryPage.Alerts_button),
        Click.on(summaryPage.Alerts_button),
    ),

    clickOnAccountBalanceButton: () => Task.where(`{0} clicks the account balance button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Account_balance_button, Is.clickable()),
        Scroll.to(summaryPage.Account_balance_button),
        Click.on(summaryPage.Account_balance_button),
    ),

    clickOnPastSurgeryButton: () => Task.where(`{0} clicks the past surgery button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Past_surgery_button, Is.clickable()),
        Scroll.to(summaryPage.Past_surgery_button),
        Click.on(summaryPage.Past_surgery_button),
    ),

    clickOnViewProfileButton: () => Task.where(`{0} clicks the view profile button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.View_profile_button, Is.clickable()),
        Scroll.to(summaryPage.View_profile_button),
        Click.on(summaryPage.View_profile_button),
    ),

    clickOnSendPortalEmailInvitationButton: () => Task.where(`{0} clicks the send profile email invitation button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Send_portal_email_button, Is.clickable()),
        Scroll.to(summaryPage.Send_portal_email_button),
        Click.on(summaryPage.Send_portal_email_button),
    ),

    clickOnReadAllNotesButton: () => Task.where(`{0} clicks the read all notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(summaryPage.Read_all_notes, Is.clickable()),
        Scroll.to(summaryPage.Read_all_notes),
        Click.on(summaryPage.Read_all_notes),
    ),

    checkIfMedicalPageOpenet: () => Task.where(`{0} checks if the medical page opened`,
    checkIfLoaderAppears(),
    waitForAllTheModalsToBeAbsent(),
    Wait.upTo(waitTime).until(summaryPage.Drug_allergies_text, Is.visible()),

),
}

// assertions

const expect = chai.expect;