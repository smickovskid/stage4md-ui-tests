import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { checkIfLoaderAppears } from '../general';
import { summaryPage } from './ui/summary-page';
import { profilePage } from './ui/profile-page';
import { patientsPage } from './ui/patients-page';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);

export const profilePageActions = {

    clickEditProfileButton: () => Task.where(`{0} clicks the edit profile button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Edit_profile_button, Is.clickable()),
        Scroll.to(profilePage.Edit_profile_button),
        Click.on(profilePage.Edit_profile_button),
    ),

    checkIfProfileOpeneed: () => Task.where(`{0} checks if the profile opened`,
    checkIfLoaderAppears(),
    waitForAllTheModalsToBeAbsent(),
    Wait.upTo(waitTime).until(profilePage.Edit_profile_button, Is.clickable()),
),

    clickEditPhotoButton: () => Task.where(`{0} clicks the edit photo button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Edit_photo_button, Is.clickable()),
        Scroll.to(profilePage.Edit_photo_button),
        Click.on(profilePage.Edit_photo_button),
    ),

    clickEditAlertsButton: () => Task.where(`{0} clicks the edit alerts button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Edit_alerts_button, Is.clickable()),
        Scroll.to(profilePage.Edit_alerts_button),
        Click.on(profilePage.Edit_alerts_button),
    ),

    clickEditCommentsButton: () => Task.where(`{0} clicks the edit comments button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Edit_comments_button, Is.clickable()),
        Scroll.to(profilePage.Edit_comments_button),
        Click.on(profilePage.Edit_comments_button),
    ),

    clickSaveCommentsButton: () => Task.where(`{0} clicks the save comments button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Save_comments_button, Is.clickable()),
        Scroll.to(profilePage.Save_comments_button),
        Click.on(profilePage.Save_comments_button),
    ),



    clickSaveEditPatientButton: () => Task.where(`{0} clicks the save edit patient button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Save_edit_patient_button, Is.clickable()),
        Scroll.to(profilePage.Save_edit_patient_button),
        Click.on(profilePage.Save_edit_patient_button),
    ),


    clickCancelEditPatientButton: () => Task.where(`{0} clicks the cancel edit patient button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(profilePage.Save_edit_patient_button, Is.clickable()),
        Scroll.to(profilePage.Save_edit_patient_button),
        Click.on(profilePage.Save_edit_patient_button),
    ),

    selectPatientStatus: (status: string) => {
        let elem: Target;
        switch (status) {
            case "Active":
                elem = profilePage.Active_status_button;
                break;
            case "Deleted":
                elem = profilePage.Deleted_status_button;
                break;
            case "Not yet seen":
                elem = profilePage.NYS_status_button;
                break;
            case "Deceased":
                elem = profilePage.Deceased_status_button;
                break;
            case "Sequestered":
                elem = profilePage.Sequestered_status_button;
                break;
        }

        return Task.where(`{0} selects the ${status} patient status`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    }
}

// assertions

const expect = chai.expect;