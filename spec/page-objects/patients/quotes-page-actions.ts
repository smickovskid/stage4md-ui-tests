import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target, BrowseTheWeb, Actor, Clear, Interaction } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { checkIfLoaderAppears } from '../general';
import { ledger } from './ui/ledger-page';
import { by } from 'protractor';
import { quotesPageElements, newQuotesElements } from './ui/quotes-page';
import { async } from 'q';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);
let actor = Actor.named("james").whoCan(BrowseTheWeb);
let quotesCount = "";

export const quotesPageActions = {

    clickNewQuoteButton: () =>
        Task.where(`{0} Clicks the new quote button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(quotesPageElements.New_quote_button, Is.clickable()),
            Scroll.to(quotesPageElements.New_quote_button),
            Click.on(quotesPageElements.New_quote_button),
        ),

    clickActiveQuotesButton: () =>
        Task.where(`{0} Clicks the active quotes button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(quotesPageElements.Active_quotes_button, Is.clickable()),
            Scroll.to(quotesPageElements.Active_quotes_button),
            Click.on(quotesPageElements.Active_quotes_button),
        ),


    setQuoteCount: () =>
        Task.where(`{0} sets the current number of quotes`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(quotesPageElements.Quote_table, Is.visible()),
            Interaction.where(`set the quote count`, async actor => {
                quotesCount = await BrowseTheWeb.as(actor).locate(quotesPageElements.Quote_table).getAttribute("childElementCount");
            })
        ),

    checkQuotecount: () =>
        Task.where(`{0} checks the current number of quotes`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(quotesPageElements.Quote_table, Is.visible()),
            Interaction.where(`set the quote count`, async actor => {
                let quotesCount1 = await BrowseTheWeb.as(actor).locate(quotesPageElements.Quote_table).getAttribute("childElementCount");
                expect(parseInt(quotesCount) + 1).to.eql(parseInt(quotesCount1));
            })

        ),

    clickAllQuotesButton: () =>
        Task.where(`{0} Clicks the all quotes button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(quotesPageElements.All_quotes_button, Is.clickable()),
            Scroll.to(quotesPageElements.All_quotes_button),
            Click.on(quotesPageElements.All_quotes_button),
        ),

    clickPreviewQuoteButton: (name: string) => {
        let elem = Target.the(`Preview button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="View"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickPrintQuoteButton: (name: string) => {
        let elem = Target.the(`Print button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Print"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickEmailQuoteButton: (name: string) => {
        let elem = Target.the(`Email button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Email"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickEditQuoteButton: (name: string) => {
        let elem = Target.the(`Edit button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Edit"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickQuoteVersionsButton: (name: string) => {
        let elem = Target.the(`Version button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Versions"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickDeleteQuoteButton: (name: string) => {
        let elem = Target.the(`Delete quote button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Delete"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickSwitchForScheduled: (name: string) => {
        let elem = Target.the(`Switch button for the ${name} quote`).located(by.xpath(`//td[contains(text(),"${name}")]/parent::*//img[@alt="Switch for Scheduled"]`))
        return Task.where(`{0} Clicks the preview button for ${name}`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },
}

export const newQuoteActons = {
    selectProvider: (name: string) => {
        let elem = Target.the(`Select the ${name} provider`).located(by.xpath(`//td[contains(text(),"${name}")]`))
        return Task.where(`{0} Clicks the ${name} provider`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    selectProcedureFromDropdown: (name: string) => {
        let elem = Target.the(`Select the ${name} procedure`).located(by.xpath(`//td[contains(text(),"${name}")]`))
        return Task.where(`{0} Clicks the ${name} procedure`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },


    selectSupplyFromDropdown: (name: string) => {
        let elem = Target.the(`Select the ${name} supply`).located(by.xpath(`//td[contains(text(),"${name}")]`))
        return Task.where(`{0} Clicks the ${name} supply`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },


    clickCloseButton: () =>
        Task.where(`{0} Clicks the close quote button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.Close_button, Is.clickable()),
            Scroll.to(newQuotesElements.Close_button),
            Click.on(newQuotesElements.Close_button),
        ),

    enterProcedureName: (name: string) =>
        Task.where(`{0} Enters ${name} in the procedure name field`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.New_procedure_name_field, Is.clickable()),
            Scroll.to(newQuotesElements.New_procedure_name_field),
            Enter.theValue(name).into(newQuotesElements.New_procedure_name_field)
        ),

    enterSupplyName: (name: string) =>
        Task.where(`{0} Enters ${name} in the supply name field`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.New_supply_name_field, Is.clickable()),
            Scroll.to(newQuotesElements.New_supply_name_field),
            Enter.theValue(name).into(newQuotesElements.New_supply_name_field)
        ),



    clickAddProcedureButton: () =>
        Task.where(`{0} Clicks the add procedure button`,
            checkIfLoaderAppears(),
            checkIfLoaderAppears(),
            checkIfLoaderAppears(),
            checkIfLoaderAppears(),
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.New_procedure_button, Is.clickable()),
            Scroll.to(newQuotesElements.New_procedure_button),
            Click.on(newQuotesElements.New_procedure_button),
        ),


    clickAddSupplyButton: () =>
        Task.where(`{0} Clicks the add procedure button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.New_supply_button, Is.clickable()),
            Scroll.to(newQuotesElements.New_supply_button),
            Click.on(newQuotesElements.New_supply_button),
        ),

    clickSaveButton: () =>
        Task.where(`{0} Clicks the save quote button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.Save_button, Is.clickable()),
            Scroll.to(newQuotesElements.Save_button),
            Click.on(newQuotesElements.Save_button),
        ),


    selectAnesthesia: (name: string) => {
        let elem: Target;
        switch (name) {
            case "general":
                elem = newQuotesElements.General_anesthesia_option;
                break;

            case "IV":
                elem = newQuotesElements.IV_sedation_option;
                break;

            case "local":
                elem = newQuotesElements.Local_option;
                break;

            case "mask":
                elem = newQuotesElements.Laryngeal_mask_option;
                break;
        }
        return Task.where(`{0} Clicks the ${name} anesthesia option`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },



    expandProviderButton: () =>
        Task.where(`{0} Clicks the expand provider button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.Expand_provider_button, Is.clickable()),
            Scroll.to(newQuotesElements.Expand_provider_button),
            Click.on(newQuotesElements.Expand_provider_button),
        ),


    expandProcedureButton: () =>
        Task.where(`{0} Clicks the expand procedure button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.Expand_procedure_button, Is.clickable()),
            Scroll.to(newQuotesElements.Expand_procedure_button),
            Click.on(newQuotesElements.Expand_procedure_button),
        ),

    expandSuppliesButton: () =>
        Task.where(`{0} Clicks the expand supplies button`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(newQuotesElements.Expand_supplies_button, Is.clickable()),
            Scroll.to(newQuotesElements.Expand_supplies_button),
            Click.on(newQuotesElements.Expand_supplies_button),
        ),


}


// assertions
const expect = chai.expect;