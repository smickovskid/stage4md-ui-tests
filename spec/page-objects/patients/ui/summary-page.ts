import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const summaryPage = {
    Allergies_button: Target.the('The allergies button').located(by.id('ClicktoClinical2')),
    Alerts_button: Target.the('The allerts button').located(by.id('btnDashToInfo2')),
    Account_balance_button: Target.the('The account button').located(by.id('ClicktoAccount')),
    Past_surgery_button: Target.the('The past surgery button').located(by.id('ClicktoClinical')),
    View_profile_button: Target.the('The view profile button').located(by.xpath('//button[text()="View Profile"]')),
    Send_portal_email_button: Target.the('Send portal email button').located(by.id('sendportalemailbtn')),
    Read_all_notes: Target.the('Read all notes button').located(by.id('btnReadNotes')),
    Drug_allergies_text: Target.the('Drug allergies text').located(by.xpath('//img[@src="/Content/Images/MedicalAlert2.png"]')),



};
