import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const ledger = {

    Issue_credit_refund_button: Target.the('Issue credit/refund button').located(by.xpath('//button[@onclick="Creditbtn()"]')),
    Product_return_button: Target.the('Product return button').located(by.xpath('//button[@onclick="Returnsbtn()"]')),

    // Issue refund modal elements
    Refund_fee_field: Target.the('Refund fee field').located(by.id('refundfee')),
    Refund_amount_field: Target.the('Refund amount field').located(by.id('refundamount')),
    Refund_note_field: Target.the('Refund note field').located(by.id('refundnote')),
    Refunding_now_button: Target.the('I am refunding now button').located(by.xpath('//input[@id="btnNow"]/parent::*')),
    Add_to_list_with_others_button: Target.the('Add to list with others button').located(by.xpath('//input[@id="btLater"]/parent::*')),
    Save_refund_button: Target.the('Save refund button').located(by.id('saverefundbtn')),

    // Issue refund reasons elements
    Patient_cancelation_refund_option: Target.the('Patient cancelation refund option').located(by.xpath('//select[@id="gRefund"]//option[text()="Patient Cancellation"]')),
    Patient_dissatisifed_refund_option: Target.the('Patient dissatisfied option').located(by.xpath('//select[@id="gRefund"]//option[text()="Patient Dissatisfied"]')),
    Procedure_not_performed_refund_option: Target.the('Procedure not performed option').located(by.xpath('//select[@id="gRefund"]//option[text()="Procedure Not Performed"]')),

    // Issue refund method elements
    CareCredit_method_refund_option: Target.the('Care credit method refund option').located(by.xpath('//select[@id="gRefundMethod"]//option[text()="CareCredit"]')),
    Cash_method_refund_option: Target.the('Cash method refund option').located(by.xpath('//select[@id="gRefundMethod"]//option[text()="Cash"]')),
    Check_method_refund_option: Target.the('Check method refund option').located(by.xpath('//select[@id="gRefundMethod"]//option[text()="Check"]')),
    Credit_card_method_refund_option: Target.the('Credit card method refund option').located(by.xpath('//select[@id="gRefundMethod"]//option[text()="Credit Card"]')),

};
