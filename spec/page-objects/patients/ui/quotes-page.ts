import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const quotesPageElements = {

    // Quotes page
    New_quote_button: Target.the('New quote button').located(by.id('quotenewbtn')),
    Active_quotes_button: Target.the('Active quotes button').located(by.id('btnActive')),
    All_quotes_button: Target.the('All quotes button').located(by.id('btnAll')),
    Quote_table: Target.the('Quote table').located(by.xpath('//table[@id="QuoteGridView_DXMainTable"]/tbody')),

};

export const newQuotesElements = {
    Close_button: Target.the('Close new quote button').located(by.id('quotecancelbtn')),
    Save_button: Target.the('Save new quote button').located(by.id('quotesavebtn')),
    Expand_provider_button: Target.the('Expand provider button').located(by.id('quoteprovider_B-1')),

    
    // Procedure elements
    Expand_procedure_button: Target.the('Expand procedure button').located(by.id('QuoteProcedureGrid_DXEditor2_B-1')),
    New_procedure_button: Target.the('New procedure button').located(by.id('btnNewProcedueHdrImg')),
    New_procedure_name_field: Target.the('New procedure name field').located(by.id('QuoteProcedureGrid_DXEditor2_I')),
    New_procedure_delete_button: Target.the('New procedure delete button').located(by.id('QuoteProcedureGrid_DXCBtn-1Img')),

    // Supplies elements
    Expand_supplies_button: Target.the('Expand supplies button').located(by.id('QuoteSuppliesGrid_DXEditor2_B-1')),
    New_supply_button: Target.the('New supply button').located(by.id('btnNewSuppliesHdr')),
    New_supply_name_field: Target.the('New supply name field').located(by.id('QuoteSuppliesGrid_DXEditor2_I')),
    New_supply_delete_button: Target.the('New procedure delete button').located(by.id('QuoteSuppliesGrid_DXCBtn-1Img')),

    //Anesthesia elements
    General_anesthesia_option: Target.the('General anesthesia option').located(by.xpath('//select[@id="anesthcd"]//option[@value="G"]')),
    IV_sedation_option: Target.the('IV sedation option').located(by.xpath('//select[@id="anesthcd"]//option[@value="I"]')),
    Local_option: Target.the('Local option').located(by.xpath('//select[@id="anesthcd"]//option[@value="L"]')),
    Laryngeal_mask_option: Target.the('Laryngeal Mask option').located(by.xpath('//select[@id="anesthcd"]//option[@value="M"]')),

}

