import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const profilePage = {

    // Profile page elements
    Edit_profile_button: Target.the('The edit profile button').located(by.id('infoeditbtn')),
    Edit_photo_button: Target.the('The edit photo button').located(by.id('infophotoeditbtn')),
    Cancel_edit_patient_button: Target.the('The cancel edit patient button').located(by.id('patienteditcancelbtn')),
    Save_edit_patient_button: Target.the('The save edit patient button').located(by.id('saveeditpatientbtn')),

    
    // Comments elements
    Edit_comments_button: Target.the('The edit comments button').located(by.id('editpatientnotebtn')),
    Save_comments_button: Target.the('The save comments button').located(by.id('savepatientnotebtn')),

    //Alerts elements
    // TODO: Add logic to create new alert
    Edit_alerts_button: Target.the('The edit alerts button').located(by.id('alerteditbtn')),
    New_alert_button: Target.the('The new + alert button').located(by.id('btnNewAlert')),


    // Patient status buttons
    Active_status_button: Target.the('The patient active status button').located(by.xpath('//input[@id="NEbtnA"]/parent::*')),
    Deleted_status_button: Target.the('The patient deleted status button').located(by.xpath('//input[@id="NEbtnI"]/parent::*')),
    NYS_status_button: Target.the('The patient not yet seen status button').located(by.xpath('//input[@id="NEbtnN"]/parent::*')),
    Deceased_status_button: Target.the('The patient deceased status button').located(by.xpath('//input[@id="NEbtnD"]/parent::*')),
    Sequestered_status_button: Target.the('The patient sequestered status button').located(by.xpath('//input[@id="NEbtnS"]/parent::*')),

};
