import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const patientsPage = {
    Patient_search_field: Target.the('The patient search field').located(by.id('SelectedPatientId_I')),
    Hourglass_button: Target.the('The hourglass search button').located(by.id('SelectedPatientId_B0')),
    Create_patient_button: Target.the('The create patient button').located(by.xpath('//td[@title="Add Patient"]')),
    Recent_patients_button: Target.the('The recent patients button').located(by.xpath('//td[@title="Recent"]')),

    // Patients header element and pages
    Summary_tab_button: Target.the('Summary button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[text()="Summary"]')),
    Profile_tab_button: Target.the('Summary button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[text()="Profile"]')),
    Quotes_tab_button: Target.the('Quotes button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[text()="Quotes"]')),
    Insurance_tab_button: Target.the('Insurance button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[text()="Insurance"]')),
    Convert_tab_button: Target.the('Convert button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[text()="Convert"]')),

    // Financials menu elements
    Financials_tab_button: Target.the('Financials button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Financials")]')),
    Ledger_dropdown_button: Target.the('Ledger button from the financials dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Ledger"]')),
    Purchase_tab_button: Target.the('Purchase button from the financials dropdown menu').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Purchase")]')),


    // Logs menu elements
    Logs_tab_button: Target.the('Logs button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Logs")]')),
    Appointments_dropdown_button: Target.the('Appointments button from the Logs dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Appointments"]')),
    Communications_dropdown_button: Target.the('Communications button from the Logs dropdown menu').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Communications")]')),
    Perscription_list_dropdown_button: Target.the('Perscription list button from the Logs dropdown menu').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Prescription List")]')),
    ePrescribing_Log_dropdown_button: Target.the('ePrescribing Log button from the Logs dropdown menu').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "ePrescribing Log")]')),


    // Clinical menu elements
    Clinical_tab_button: Target.the('Clinical button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Clinical")]')),
    Read_notes_dropdown_button: Target.the('Read notes button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Read Notes"]')),
    Write_notes_dropdown_button: Target.the('Write notes button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Write Notes"]')),
    Drafts_dropdown_button: Target.the('Drafts button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Drafts "]')),
    Vital_signs_dropdown_button: Target.the('Vital signs button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Vital Signs"]')),
    Medical_history_dropdown_button: Target.the('Medical History button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Medical History"]')),
    Review_of_systems_dropdown_button: Target.the('Review of Systems button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="Review of Systems"]')),
    ePerscribe_dropdown_button: Target.the('ePrescribe button from the Clinical dropdown menu').located(by.xpath('//ul[@class="dropdown-menu"]//a[text()="ePrescribe"]')),

    // Forms & Scans menu elements
    Forms_and_scans_tab_button: Target.the('Forms & scans button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Forms & Scans")]')),
    Forms_View_dropdown_button: Target.the('View button from the Forms & scans dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Forms")]/parent::*//a[text()="View"]')),
    Forms_generate_forms_dropdown_button: Target.the('Generate Forms button from the Forms & scans dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Forms")]/parent::*//a[text()="Generate Forms"]')),
    Forms_direct_scan_dropdown_button: Target.the('Direct Scan button from the Forms & scans dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Forms")]/parent::*//a[text()="Direct Scan"]')),

    // Clinical menu elements
    Photos_tab_button: Target.the('Clinical button from the tab').located(by.xpath('//ul[@id="PatientTabs"]//a[contains(text(), "Photos")]')),
    Photos_view_dropdown_button: Target.the('View button from the Photos dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Photos")]/parent::*//a[text()="View"]')),
    Photos_Photo_search_dropdown_button: Target.the('Photo Search button from the Photos dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Photos")]/parent::*//a[text()="Photo Search"]')),
    Photos_Collections_dropdown_button: Target.the('Collections button from the Photos dropdown menu').located(by.xpath('//a[@class="dropdown-toggle"][contains(text(), "Photos")]/parent::*//a[text()="Collections"]')),





};
