import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const readNotesElements = {

    Read_all_button: Target.the('Read all notes button').located(by.xpath('//button[contains(text(),"Read All")]')),
    Only_clinical_button: Target.the('Only clinical notes button').located(by.xpath('//button[contains(text(),"Only Clinical")]')),
    Only_non_clinical_button: Target.the('Only non clinical notes button').located(by.xpath('//button[contains(text(),"Only Non-Clinical")]')),
    Last_H_P_button: Target.the('Last H & P button').located(by.xpath('//button[contains(text(),"Last H & P")]')),
    All_H_P_button: Target.the('All H & P button').located(by.xpath('//button[contains(text(),"All H & P")]')),
    Last_operative_report_button: Target.the('Last operative report button').located(by.xpath('//button[contains(text(),"Last Operative Report")]')),
    All_operative_reports_button: Target.the('All operative reports button').located(by.xpath('//button[contains(text(),"All Operative Reports")]')),

};
