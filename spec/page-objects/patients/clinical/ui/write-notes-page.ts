import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const writeNotesElements = {

    Email_insta_note_button: Target.the('Email insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Email")]')),
    H_P_insta_note_button: Target.the('H & P insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"History & Physical")]')),
    Medication_insta_note_button: Target.the('Medication note insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Medication Note")]')),
    Miscellaneous_clinical_insta_note_button: Target.the('Misc. clinical insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Miscellaneous Clinical")]')),
    Office_visit_insta_note_button: Target.the('Office visit insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Office Visit")]')),
    Operative_report_insta_note_button: Target.the('Operative report insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Operative Report")]')),
    Preoperative_visit_insta_note_button: Target.the('Preoperative visit insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Preoperative Visit")]')),
    Private_insta_note_button: Target.the('Private insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Private")]')),
    Procedure_insta_note_button: Target.the('Procedure insta note button').located(by.xpath('//div[@id="ChartNoteTypeByProvider_D"]//td[contains(text(),"Procedure")]')),


    // Note editor elements
    Save_note_button: Target.the('Save note button').located(by.xpath('//span[text()="Save"]')),
    Cancel_note_button: Target.the('Cancel note button').located(by.xpath('//span[text()="Cancel"]')),
    Cancel_ok_button: Target.the('Cancel ok note button').located(by.xpath('//button[text()="OK"]')),
    Cancel_dialog_button: Target.the('Cancel cancel note button').located(by.xpath('//button[text()="Cancel"]')),


    // No patient elements
    Delete_note_button: Target.the('Delete note button').located(by.xpath('//*[@id="ApptsForAllNotes_DXDataRow0"]//img[@title="No Note"]')),




};
