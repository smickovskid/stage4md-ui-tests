import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const medicalHistoryPageElements = {

    Cancel_top_button: Target.the('The cancel button on top').located(by.id('medhxcancelbtn')),
    Save_top_button: Target.the('The save button on top').located(by.id('medhxsavebtn')),

    // Drug allergies elements
    Drug_allergies_yes_button: Target.the('The drug allergies yes button').located(by.xpath('//input[@id="btnHasAllergies"]/parent::*')),
    Drug_allergies_NKDA_button: Target.the('The drug allergies NKDA button').located(by.xpath('//input[@id="btnNKDA"]/parent::*')),
    Drug_allergies_unknown_button: Target.the('The drug allergies unknown button').located(by.xpath('//input[@id="btnAllergiesUnk"]/parent::*')),
    Drug_allergies_plus_button: Target.the('The drug allergies plus button').located(by.id('btnNewAllergy')),
    Drug_allergies_new_field: Target.the('The drug allergies new field').located(by.id('AllergyGridView_DXEditor1_I')),
    Asmanex_option: Target.the('The Aspirin option').located(by.xpath('//td[contains(text(),"manex")]')),


    // Latex allergies elements
    Latex_allergy_yes_button: Target.the('The latex allergy yes button').located(by.xpath('//input[@id="btnLatexYes"]/parent::*')),
    Latex_allergy_no_button: Target.the('The latex allergy no button').located(by.xpath('//input[@id="btnLatexNo"]/parent::*')),

    // Adhesive allergies elements
    Adhesive_allergy_yes_button: Target.the('The adhesive allergy yes button').located(by.xpath('//input[@id="btnTapeYes"]/parent::*')),
    Adhesive_allergy_no_button: Target.the('The adhesive allergy no button').located(by.xpath('//input[@id="btnTapeNo"]/parent::*')),

    // Medications elements
    Medications_yes_button: Target.the('The medications yes button').located(by.xpath('//input[@id="btnHasMedications"]/parent::*')),
    Medications_no_button: Target.the('The medications no button').located(by.xpath('//input[@id="btnNoMedications"]/parent::*')),
    Medications_unknown_button: Target.the('The medications unknown button').located(by.xpath('//input[@id="btnMedicationsUnk"]/parent::*')),
    Medications_plus_button: Target.the('Medications plus button').located(by.id('btnNewMedication_CD')),
    Medications_new_field: Target.the('Medications new field').located(by.id('MedicationGridView_DXEditor1_I')),
    Medications_100ug_option: Target.the('100ug option').located(by.xpath('//td[contains(text(),"100 ug")]')),


    // Past medical history elements
    Past_medical_history_yes_button: Target.the('The past medical history yes button').located(by.xpath('//input[@id="btnHasPMH"]/parent::*')),
    Past_medical_history_nonC_button: Target.the('The past medical history nonC button').located(by.xpath('//input[@id="btnNoPMH"]/parent::*')),
    Past_medical_history_unknown_button: Target.the('The past medical history unknown button').located(by.xpath('//input[@id="btnPMHUnk"]/parent::*')),
    Past_medical_history_plus_button: Target.the('Past medical history plus button').located(by.id('btnNewMedicalHdr')),
    Past_medical_history_new_field: Target.the('Past medical history new field').located(by.id('PMHxGridView_DXEditor3_I')),
    Past_medical_history_hodgkin_disease_option: Target.the('Hodkings disease option').located(by.xpath('//td[contains(text(),"Ast")]')),


    // Surgical history elements
    Surgical_history_yes_button: Target.the('The surgical history yes button').located(by.xpath('//input[@id="btnHasPSH"]/parent::*')),
    Surgical_history_nonC_button: Target.the('The surgical history nonC button').located(by.xpath('//input[@id="btnNoPSH"]/parent::*')),
    Surgical_history_unknown_button: Target.the('The surgical history unknown button').located(by.xpath('//input[@id="btnPSHUnk"]/parent::8')),
    Surgical_history_plus_button: Target.the('Surgical history plus button').located(by.id('btnNewSurgery')),
    Surgical_history_new_field: Target.the('Surgical history new field').located(by.id('PastSurgeryGridView_DXEditor3_I')),
    Surgical_history_blepharopl_option: Target.the('Blepharopl option').located(by.xpath('//td[contains(text(),"Blepharoplasty")]')),

    // Social history tobacco use elements
    SH_tobacco_use_yes_button: Target.the('The social history tobacco use yes button').located(by.xpath('//input[@id="btnSmokingYes"]/parent::*')),
    SH_tobacco_use_no_button: Target.the('The social history tobacco use no button').located(by.xpath('//input[@id="btnSmokingNo"]/parent::*')),
    SH_tobacco_use_comments_field: Target.the('The tobacco use comments field').located(by.id('tobacconote')),

    // Social history acohol use elements
    SH_alcohol_use_yes_button: Target.the('The social history alcohol use yes button').located(by.xpath('//input[@id="btnAlcoholYes"]/parent::*')),
    SH_alcohol_use_no_button: Target.the('The social history alcohol use no button').located(by.xpath('//input[@id="btnAlcoholNo"]/parent::*')),
    SH_alcohol_use_comments_field: Target.the('The alcohol use comments field').located(by.id('alcoholnote')),

    // Social history drug use elements
    SH_drug_use_yes_button: Target.the('The social history drug use yes button').located(by.xpath('//input[@id="btnDrugsYes"]/parent::8')),
    SH_drug_use_no_button: Target.the('The social history drug use no button').located(by.xpath('//input[@id="btnDrugsNo"]/parent::*')),
    SH_drug_use_comments_field: Target.the('The drug use comments field').located(by.id('drugsnote')),

    // Family history elements
    FH_bleeding_yes_button: Target.the('The family history bleeding yes button').located(by.xpath('//input[@id="FBleedingYes"]/parent::*')),
    FH_bleeding_no_button: Target.the('The family history bleeding no button').located(by.xpath('//input[@id="FBleedingNo"]/parent::*')),
    FH_bleeding_comments_field: Target.the('Family history bleeding comments field').located(by.id('FHxBleedingNote')),

    FH_anesthesia_yes_button: Target.the('The family history anesthesia yes button').located(by.xpath('//input[@id="FAnesthYes"]/parent::*')),
    FH_anesthesia_no_button: Target.the('The family history anesthesia no button').located(by.xpath('//input[@id="FAnesthNo"]/parent::*')),
    FH_anesthesia_comments_field: Target.the('Family history anesthesia comments field').located(by.id('FHxAnesthNote')),
    FH_other_FH_comments_field: Target.the('Family history other Family history comments field').located(by.id('familyHxNote')),


};
