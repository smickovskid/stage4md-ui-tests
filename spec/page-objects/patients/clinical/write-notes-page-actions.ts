import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target, BrowseTheWeb, Actor, Clear, Interaction } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { readNotesElements } from './ui/read-notes-page';
import { by, element } from 'protractor';
import { writeNotesElements } from './ui/write-notes-page';
import { checkIfLoaderAppears } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);
let actor = Actor.named("james").whoCan(BrowseTheWeb);

let writeNotesChildrenCount;
export const writeNotesPageActions = {

    clickEmailInstaNoteButton: () => Task.where(`{0} clicks the Email insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Email_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Email_insta_note_button),
        Click.on(writeNotesElements.Email_insta_note_button)
    ),

    clickHPInstaNoteButton: () => Task.where(`{0} clicks the  H & P insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.H_P_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.H_P_insta_note_button),
        Click.on(writeNotesElements.H_P_insta_note_button)
    ),

    clickDeleteNoteFromAppointment: () => Task.where(`{0} clicks the  delete note from appointment`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Delete_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Delete_note_button),
        Click.on(writeNotesElements.Delete_note_button)
    ),

    setChildrenCount: () => {
        let elem = Target.the("Appointment notes table element").located(by.xpath('//*[@id="ApptsForAllNotes_DXMainTable"]/tbody'))
        return Task.where(`{0} checks the number of children`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(`Get the children count`, async actor => {
                writeNotesChildrenCount = await BrowseTheWeb.as(actor).locate(elem).getAttribute('childElementCount');
            })
        )
    },

    checkCildrenCount: () => {
        let elem = Target.the("Appointment notes table").located(by.xpath('//*[@id="ApptsForAllNotes_DXMainTable"]/tbody'))
        return Task.where(`{0} clicks the  delete note from appointment`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(`Get the children count`, async actor => {
                let count = await BrowseTheWeb.as(actor).locate(elem).getAttribute('childElementCount');
                expect(count).to.eql(writeNotesChildrenCount - 1)
            })
        )
    },


    clickMedicationInstaNoteButton: () => Task.where(`{0} clicks the Medication insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Medication_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Medication_insta_note_button),
        Wait.for(Duration.ofMillis(1500)),
        Click.on(writeNotesElements.Medication_insta_note_button)
    ),

    clickMiscInstaNoteButton: () => Task.where(`{0} clicks the  Misc. insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Miscellaneous_clinical_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Miscellaneous_clinical_insta_note_button),
        Click.on(writeNotesElements.Miscellaneous_clinical_insta_note_button)
    ),

    clickOfficeVisitInstaNoteButton: () => Task.where(`{0} clicks the  office visit insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Office_visit_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Office_visit_insta_note_button),
        Click.on(writeNotesElements.Office_visit_insta_note_button)
    ),

    clickOperativeReportInstaNoteButton: () => Task.where(`{0} clicks the  Operative report insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Operative_report_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Operative_report_insta_note_button),
        Click.on(writeNotesElements.Operative_report_insta_note_button)
    ),

    clickPreoperativeVisitInstaNoteButton: () => Task.where(`{0} clicks the  Preoperative visit insta note button button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Preoperative_visit_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Preoperative_visit_insta_note_button),
        Click.on(writeNotesElements.Preoperative_visit_insta_note_button)
    ),

    clickPrivateInstaNoteButton: () => Task.where(`{0} clicks the  private insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Private_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Private_insta_note_button),
        Click.on(writeNotesElements.Private_insta_note_button)
    ),

    clickProcedureInstaNoteButton: () => Task.where(`{0} clicks the  Procedure insta note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Procedure_insta_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Procedure_insta_note_button),
        Click.on(writeNotesElements.Procedure_insta_note_button)
    ),

    clickSave_note_Button: () => Task.where(`{0} clicks theSave note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Save_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Save_note_button),
        Click.on(writeNotesElements.Save_note_button)
    ),

    clickCancelNoteButton: () => Task.where(`{0} clicks the Cancel note button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Cancel_note_button, Is.clickable()),
        Scroll.to(writeNotesElements.Cancel_note_button),
        Click.on(writeNotesElements.Cancel_note_button)
    ),

    clickOkFromModalButton: () => Task.where(`{0} clicks the Cancel ok button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Cancel_ok_button, Is.clickable()),
        Scroll.to(writeNotesElements.Cancel_ok_button),
        Click.on(writeNotesElements.Cancel_ok_button)
    ),

    clickCancelFromModalButton: () => Task.where(`{0} clicks the Cancel cancel button for the modal`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(writeNotesElements.Cancel_dialog_button, Is.clickable()),
        Scroll.to(writeNotesElements.Cancel_dialog_button),
        Click.on(writeNotesElements.Cancel_dialog_button)
    ),
}

// assertions

const expect = chai.expect;