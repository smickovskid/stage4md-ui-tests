import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target, BrowseTheWeb, Actor, Clear, Interaction } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { readNotesElements } from './ui/read-notes-page';
import { by, element } from 'protractor';
import { checkIfLoaderAppears } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);
let actor = Actor.named("james").whoCan(BrowseTheWeb);
export const readNotesPageActions = {

    clickReadAllNotesButton: () => Task.where(`{0} clicks the read all notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.Read_all_button, Is.clickable()),
        Scroll.to(readNotesElements.Read_all_button),
        Click.on(readNotesElements.Read_all_button)
    ),
    clickOnlyClinicalNotesButton: () => Task.where(`{0} clicks the  only clinical notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.Only_clinical_button, Is.clickable()),
        Scroll.to(readNotesElements.Only_clinical_button),
        Click.on(readNotesElements.Only_clinical_button)
    ),
    clickOnlyNonClinicalNotesButton: () => Task.where(`{0} clicks the  only non clinical notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.Only_non_clinical_button, Is.clickable()),
        Scroll.to(readNotesElements.Only_non_clinical_button),
        Click.on(readNotesElements.Only_non_clinical_button)
    ),
    clickLastHAndPNotesButton: () => Task.where(`{0} clicks the  last H & P notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.Last_H_P_button, Is.clickable()),
        Scroll.to(readNotesElements.Last_H_P_button),
        Click.on(readNotesElements.Last_H_P_button)
    ),
    clickAllHAndPNotesButton: () => Task.where(`{0} clicks the  All h & P notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.All_H_P_button, Is.clickable()),
        Scroll.to(readNotesElements.All_H_P_button),
        Click.on(readNotesElements.All_H_P_button)
    ),
    clickLastOperativeReportNotesButton: () => Task.where(`{0} clicks the  last operative report notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.Last_operative_report_button, Is.clickable()),
        Scroll.to(readNotesElements.Last_operative_report_button),
        Click.on(readNotesElements.Last_operative_report_button)
    ),
    clickAllOpeartiveReportsNotesButton: () => Task.where(`{0} clicks the  all operative reports notes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.All_operative_reports_button, Is.clickable()),
        Scroll.to(readNotesElements.All_operative_reports_button),
        Click.on(readNotesElements.All_operative_reports_button)
    ),


    checkNotesCount: (count: string) => Task.where(`{0} check whether the count of notes is ${count}`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(readNotesElements.All_operative_reports_button, Is.clickable()),
        Interaction.where(`{0} checks the count of the notes`, async actor => {
            let childCount = await element(by.xpath(`//table[@id="NotesListBox_DXMainTable"]//tbody`)).getAttribute("childElementCount");
            expect(childCount).to.eql(count, "Note count not matching");
        })
    ),
}

// assertions

const expect = chai.expect;