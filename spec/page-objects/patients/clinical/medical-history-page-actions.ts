import { Scroll, Task, Wait, Is, Enter, Duration, Click, Target, BrowseTheWeb, Actor } from 'serenity-js/lib/screenplay-protractor';
import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { medicalHistoryPageElements } from './ui/medical-history-page';
import { checkIfLoaderAppears } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
chai.use(chaiAsPromised);
export const waitTime = Duration.ofMillis(30000);
let actor = Actor.named("james").whoCan(BrowseTheWeb);
export const medicalHistoryPageActions = {

    enterDrugAllergy: (text: string) => Task.where(`{0} enters ${text} as allergy`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Drug_allergies_new_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Drug_allergies_new_field),
        Click.on(medicalHistoryPageElements.Drug_allergies_new_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Drug_allergies_new_field)
    ),

    enterMedication: (text: string) => Task.where(`{0} enters ${text} as allergy`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_new_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_new_field),
        Click.on(medicalHistoryPageElements.Medications_new_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Medications_new_field)
    ),

    enterPastMedicalHistory: (text: string) => Task.where(`{0} enters ${text} as past medical history`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_new_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_new_field),
        Click.on(medicalHistoryPageElements.Past_medical_history_new_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Past_medical_history_new_field)
    ),

    enterTobaccoUseComments: (text: string) => Task.where(`{0} enters ${text} as tobacco use comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_tobacco_use_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_tobacco_use_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Medications_new_field)
    ),

    enterDrugUseComments: (text: string) => Task.where(`{0} enters ${text} as drug use comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_drug_use_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_drug_use_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Medications_new_field)
    ),

    enterAlcoholUseComments: (text: string) => Task.where(`{0} enters ${text} as alcohol use comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_alcohol_use_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_alcohol_use_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Medications_new_field)
    ),

    enterBleedingComments: (text: string) => Task.where(`{0} enters ${text} as bleeding comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_bleeding_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_bleeding_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.FH_bleeding_comments_field)
    ),

    enterAnesthesiaComments: (text: string) => Task.where(`{0} enters ${text} as bleeding comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_anesthesia_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_anesthesia_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.FH_anesthesia_comments_field)
    ),

    enterOtherFamilyHistoryComments: (text: string) => Task.where(`{0} enters ${text} as other family history comments field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_other_FH_comments_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_other_FH_comments_field),
        Enter.theValue(text).into(medicalHistoryPageElements.FH_other_FH_comments_field)
    ),

    enterSurgicalHistory: (text: string) => Task.where(`{0} enters ${text} as past medical history`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_new_field, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_new_field),
        Click.on(medicalHistoryPageElements.Surgical_history_new_field),
        Enter.theValue(text).into(medicalHistoryPageElements.Surgical_history_new_field)
    ),

    clickOnblepharoplOption: () => Task.where(`{0} clicks the blepharopl option`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_blepharopl_option, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_blepharopl_option),
        Click.on(medicalHistoryPageElements.Surgical_history_blepharopl_option)
    ),

    clickOnAsmanexOption: () => Task.where(`{0} clicks the  aspirin option`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Asmanex_option, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Asmanex_option),
        Click.on(medicalHistoryPageElements.Asmanex_option)
    ),


    clickHodkinsDiseaseOption: () => Task.where(`{0} clicks the  hodkins disease option`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_hodgkin_disease_option, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_hodgkin_disease_option),
        Click.on(medicalHistoryPageElements.Past_medical_history_hodgkin_disease_option)
    ),
    clickPlusDrugAllergyButton: () => Task.where(`{0} clicks the  plus new allergy button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Drug_allergies_plus_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Drug_allergies_plus_button),
        Click.on(medicalHistoryPageElements.Drug_allergies_plus_button)
    ),

    clickPlusSurgicalHistoryButton: () => Task.where(`{0} clicks the  plus new allergy button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_plus_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_plus_button),
        Click.on(medicalHistoryPageElements.Surgical_history_plus_button)
    ),

    click100ugOption: () => Task.where(`{0} clicks the  100 ug option`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_100ug_option, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_100ug_option),
        Click.on(medicalHistoryPageElements.Medications_100ug_option)
    ),

    clickPlusMedicalHistoryButton: () => Task.where(`{0} clicks the  plus medical history button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_plus_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_plus_button),
        Click.on(medicalHistoryPageElements.Past_medical_history_plus_button)
    ),

    clickPlusMedicationsButton: () => Task.where(`{0} clicks the  plus new medication button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_plus_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_plus_button),
        Click.on(medicalHistoryPageElements.Medications_plus_button)
    ),

    clickCancelTopButton: () => Task.where(`{0} clicks the  Cancel top button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Cancel_top_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Cancel_top_button),
        Click.on(medicalHistoryPageElements.Cancel_top_button)
    ),

    clickSaveTopButton: () => Task.where(`{0} clicks the Save top button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Save_top_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Save_top_button),
        Click.on(medicalHistoryPageElements.Save_top_button)
    ),

    clickDrugAllergiesYesButton: () => Task.where(`{0} clicks the Drug allergies yes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Drug_allergies_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Drug_allergies_yes_button),
        Click.on(medicalHistoryPageElements.Drug_allergies_yes_button)
    ),

    clickDrugAllergiesNKDAButton: () => Task.where(`{0} clicks the Drug allergies NKDA button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Drug_allergies_NKDA_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Drug_allergies_NKDA_button),
        Click.on(medicalHistoryPageElements.Drug_allergies_NKDA_button)
    ),

    clickDrugAllergiesUnknownButton: () => Task.where(`{0} clicks the Drug allergies unknown button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Drug_allergies_unknown_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Drug_allergies_unknown_button),
        Click.on(medicalHistoryPageElements.Drug_allergies_unknown_button)
    ),

    clickLatexAllergyYesButton: () => Task.where(`{0} clicks the Latex allergy yes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Latex_allergy_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Latex_allergy_yes_button),
        Click.on(medicalHistoryPageElements.Latex_allergy_yes_button)
    ),

    clickLatexAllergyNoButton: () => Task.where(`{0} clicks the Latex allergy no button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Latex_allergy_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Latex_allergy_no_button),
        Click.on(medicalHistoryPageElements.Latex_allergy_no_button)
    ),

    clickAdhesiveAllergyYesButton: () => Task.where(`{0} clicks the Adhesive allergy yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Adhesive_allergy_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Adhesive_allergy_yes_button),
        Click.on(medicalHistoryPageElements.Adhesive_allergy_yes_button)
    ),

    clickAdhesiveAllergyNoButton: () => Task.where(`{0} clicks the Adhesive allergy no button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(medicalHistoryPageElements.Adhesive_allergy_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Adhesive_allergy_no_button),
        Click.on(medicalHistoryPageElements.Adhesive_allergy_no_button)
    ),

    clickMedicationsYesButton: () => Task.where(`{0} clicks the Medications yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_yes_button),
        Click.on(medicalHistoryPageElements.Medications_yes_button)
    ),

    clickMedicationsNoButton: () => Task.where(`{0} clicks the Medications no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_no_button),
        Click.on(medicalHistoryPageElements.Medications_no_button)
    ),

    clickMedicationsUnknownButton: () => Task.where(`{0} clicks the Medications unknown button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Medications_unknown_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Medications_unknown_button),
        Click.on(medicalHistoryPageElements.Medications_unknown_button)
    ),

    clickPastMedicalHistoryYesButton: () => Task.where(`{0} clicks the Past medical history yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_yes_button),
        Click.on(medicalHistoryPageElements.Past_medical_history_yes_button)
    ),

    clickPastMedicalHistoryNonCButton: () => Task.where(`{0} clicks the Past medical history nonC button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_nonC_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_nonC_button),
        Click.on(medicalHistoryPageElements.Past_medical_history_nonC_button)
    ),

    clickPastMedicalHistoryUnknownButton: () => Task.where(`{0} clicks the Past medical history unknown button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Past_medical_history_unknown_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Past_medical_history_unknown_button),
        Click.on(medicalHistoryPageElements.Past_medical_history_unknown_button)
    ),

    clickSurgicalHistoryYesButton: () => Task.where(`{0} clicks the Surgical history yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_yes_button),
        Click.on(medicalHistoryPageElements.Surgical_history_yes_button)
    ),

    clickSurgicalHistoryNonCButton: () => Task.where(`{0} clicks the Surgical history nonC button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_nonC_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_nonC_button),
        Click.on(medicalHistoryPageElements.Surgical_history_nonC_button)
    ),

    clickSurgicalHistoryUnknownButton: () => Task.where(`{0} clicks the Surgical history unknown button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.Surgical_history_unknown_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.Surgical_history_unknown_button),
        Click.on(medicalHistoryPageElements.Surgical_history_unknown_button)
    ),

    clickSHTobaccoUseYesButton: () => Task.where(`{0} clicks the SH tobacco use yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_tobacco_use_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_tobacco_use_yes_button),
        Click.on(medicalHistoryPageElements.SH_tobacco_use_yes_button)
    ),

    clickSHTobaccoUseNoButton: () => Task.where(`{0} clicks the SH tobacco use no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_tobacco_use_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_tobacco_use_no_button),
        Click.on(medicalHistoryPageElements.SH_tobacco_use_no_button)
    ),

    clickSHAlcoholUseYesButton: () => Task.where(`{0} clicks the SH alcohol use yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_alcohol_use_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_alcohol_use_yes_button),
        Click.on(medicalHistoryPageElements.SH_alcohol_use_yes_button)
    ),

    clickSHAlcoholUseNoButton: () => Task.where(`{0} clicks the SH alcohol use no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_alcohol_use_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_alcohol_use_no_button),
        Click.on(medicalHistoryPageElements.SH_alcohol_use_no_button)
    ),

    clickSHDrugUseYesButton: () => Task.where(`{0} clicks the SH drug use yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_drug_use_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_drug_use_yes_button),
        Click.on(medicalHistoryPageElements.SH_drug_use_yes_button)
    ),

    clickSHDrugUseNoButton: () => Task.where(`{0} clicks the SH drug use no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.SH_drug_use_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.SH_drug_use_no_button),
        Click.on(medicalHistoryPageElements.SH_drug_use_no_button)
    ),

    clickFHBleedingYesButton: () => Task.where(`{0} clicks the FH bleeding yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_bleeding_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_bleeding_yes_button),
        Click.on(medicalHistoryPageElements.FH_bleeding_yes_button)
    ),

    clickFHBleedingNoButton: () => Task.where(`{0} clicks the FH bleeding no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_bleeding_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_bleeding_no_button),
        Click.on(medicalHistoryPageElements.FH_bleeding_no_button)
    ),

    clickFHBnesthesiaYesButton: () => Task.where(`{0} clicks the FH anesthesia yes button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_anesthesia_yes_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_anesthesia_yes_button),
        Click.on(medicalHistoryPageElements.FH_anesthesia_yes_button)
    ),

    clickFHAnesthesiaNoButton: () => Task.where(`{0} clicks the FH anesthesia no button`,
        checkIfLoaderAppears(), waitForAllTheModalsToBeAbsent(), Wait.upTo(waitTime).until(medicalHistoryPageElements.FH_anesthesia_no_button, Is.visible()),
        Scroll.to(medicalHistoryPageElements.FH_anesthesia_no_button),
        Click.on(medicalHistoryPageElements.FH_anesthesia_no_button)
    ),

}


// assertions
const expect = chai.expect;