import { Scroll, See, Task, Text, Wait, Is, Enter, Duration, Click, Target, Interaction, BrowseTheWeb } from 'serenity-js/lib/screenplay-protractor';

import { patientsPage } from './ui/patients-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { browser, by } from 'protractor';
import { generalElementsActions, checkIfLoaderAppears } from '../general';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);

export const patientsPageActions = {

    clickOnSummaryButton: () => Task.where(`{0} clicks the summary button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Summary_tab_button, Is.clickable()),
        Scroll.to(patientsPage.Summary_tab_button),
        Click.on(patientsPage.Summary_tab_button),
    ),

    clickNewPatientButton: () => Task.where(`{0} clicks the new patient button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Create_patient_button, Is.clickable()),
        Scroll.to(patientsPage.Create_patient_button),
        Click.on(patientsPage.Create_patient_button),
    ),

    enterTextInSearchField: (text: string) => Task.where(`{0} enters the text ${text} in the patients search field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Patient_search_field, Is.visible()),
        Scroll.to(patientsPage.Patient_search_field),
        Enter.theValue(text).into(patientsPage.Patient_search_field)
    ),

    clickSearchButton: () => Task.where(`{0} clicks the search button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Hourglass_button, Is.clickable()),
        Scroll.to(patientsPage.Hourglass_button),
        Click.on(patientsPage.Hourglass_button),
    ),

    selectPatientFromMatches: (patientName: string) => {
        let elem = Target.the(`${patientName} from the search matches`).located(by.xpath(`//em[contains(text(), "${patientName}")]/parent::*`))
        return Task.where(`{0} selects the ${patientName} patient`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    clickRecentPatientsButton: () => Task.where(`{0} clicks the recent patients button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Recent_patients_button, Is.clickable()),
        Scroll.to(patientsPage.Recent_patients_button),
        Click.on(patientsPage.Recent_patients_button),
    ),

    clickOnProfileButton: () => Task.where(`{0} clicks the profile button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Profile_tab_button, Is.clickable()),
        Scroll.to(patientsPage.Profile_tab_button),
        Click.on(patientsPage.Profile_tab_button),
    ),

    clickOnQuotesButton: () => Task.where(`{0} clicks the quotes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Quotes_tab_button, Is.clickable()),
        Scroll.to(patientsPage.Quotes_tab_button),
        Click.on(patientsPage.Quotes_tab_button),
    ),

    clickOnInsuranceButton: () => Task.where(`{0} clicks the insurance button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Insurance_tab_button, Is.clickable()),
        Scroll.to(patientsPage.Insurance_tab_button),
        Click.on(patientsPage.Insurance_tab_button),
    ),

    clickOnConvertButton: () => Task.where(`{0} clicks the insurance button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(patientsPage.Convert_tab_button, Is.clickable()),
        Scroll.to(patientsPage.Convert_tab_button),
        Click.on(patientsPage.Convert_tab_button),
    ),


    clickButtonFromDropdownMenu: (buttonName: string) => {
        buttonName = buttonName.toLowerCase();
        let elem: Target;
        switch (buttonName) {
            case "ledger":
                elem = patientsPage.Ledger_dropdown_button;
                break;
            case "purchase":
                elem = patientsPage.Purchase_tab_button;
                break;
            case "read notes":
                elem = patientsPage.Read_notes_dropdown_button;
                break;
            case "write notes":
                elem = patientsPage.Write_notes_dropdown_button;
                break;

            case "drafts":
                elem = patientsPage.Drafts_dropdown_button;
                break;

            case "vital signs":
                elem = patientsPage.Vital_signs_dropdown_button;
                break;

            case "medical history":
                elem = patientsPage.Medical_history_dropdown_button;
                break;

            case "review of systems":
                elem = patientsPage.Review_of_systems_dropdown_button;
                break;

            case "eprescribe":
                elem = patientsPage.ePerscribe_dropdown_button;
                break;

            case "appointments":
                elem = patientsPage.Appointments_dropdown_button;
                break;

            case "communications":
                elem = patientsPage.Communications_dropdown_button;
                break;

            case "prescription list":
                elem = patientsPage.Perscription_list_dropdown_button;
                break;

            case "eprescribing log":
                elem = patientsPage.ePrescribing_Log_dropdown_button;
                break;

            case "forms view":
                elem = patientsPage.Forms_View_dropdown_button;
                break;

            case "generate forms":
                elem = patientsPage.Forms_generate_forms_dropdown_button;
                break;

            case "direct scan":
                elem = patientsPage.Forms_direct_scan_dropdown_button;
                break;


            case "Photos View":
                elem = patientsPage.Photos_view_dropdown_button;
                break;

            case "Photo Search":
                elem = patientsPage.Photos_Photo_search_dropdown_button;
                break;

            case "Collections":
                elem = patientsPage.Photos_Collections_dropdown_button;
                break;


        }

        return Task.where(`{0} clicks element from the expanded dropdown menu in the patients header`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Click.on(elem)
        )
    },


    hoverOverTabButton: (buttonName: string) => {
        buttonName = buttonName.toLowerCase();
        let elem: Target;
        switch (buttonName) {
            case "financials":
                elem = patientsPage.Financials_tab_button;
                break;
            case "logs":
                elem = patientsPage.Logs_tab_button;
                break;
            case "clinical":
                elem = patientsPage.Clinical_tab_button;
                break;
            case "photos":
                elem = patientsPage.Photos_tab_button;
                break;
        }

        return Task.where(`{0} hovers over the dropdown buttons in the patients header`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(`{0} hovers over the ${buttonName} button`,
                async actor => {
                    let elemLoc = await BrowseTheWeb.as(actor).locate(elem);
                    await browser.actions()
                        .mouseMove(elemLoc)
                        .perform()
                    await elemLoc.click();
                }
            )
        )
    }
}

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
