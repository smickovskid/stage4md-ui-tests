import { Scroll, See, Task, Text, Wait, Is, Enter, Target, Interaction, BrowseTheWeb, Click } from 'serenity-js/lib/screenplay-protractor';

import { practicePageElements } from './ui/practice-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { checkIfLoaderAppears, james } from '../general';
import { waitForAllTheModalsToBeAbsent } from '../schedule';
import { waitTime } from '../patients/clinical/medical-history-page-actions';
import { browser, by } from 'protractor';
chai.use(chaiAsPromised);

export const practicePageActions = {

    hoverOverTabButton: (buttonName: string) => {
        buttonName = buttonName.toLowerCase();
        let elem: Target;
        switch (buttonName) {
            case "admin":
                elem = practicePageElements.Admin_button;
                break;
            case "charting":
                elem = practicePageElements.Charting_button;
                break;
            case "products":
                elem = practicePageElements.Products_button;
                break;
            case "integrations":
                elem = practicePageElements.Integrations_button;
                break;

            case "subscription":
                elem = practicePageElements.Subscriptions_button;
                break;
        }

        return Task.where(`{0} hovers over the dropdown buttons in the patients header`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Interaction.where(`{0} hovers over the ${buttonName} button`,
                async actor => {
                    let elemLoc = await BrowseTheWeb.as(actor).locate(elem);
                    await browser.actions()
                        .mouseMove(elemLoc)
                        .perform()
                    await elemLoc.click();
                }
            )
        )
    },

    clickButtonFromDropdownMenu: (buttonName: string) => {
        buttonName = buttonName.toLowerCase();
        let elem: Target;
        switch (buttonName) {
            case "offices":
                elem = practicePageElements.Offices_button;
                break;
            case "doctors and staff":
                elem = practicePageElements.Doctors_and_staff_button;
                break;
            case "appointment types":
                elem = practicePageElements.Appointment_types_button;
                break;
            case "referring doctors":
                elem = practicePageElements.Referring_doctors_button;
                break;

            case "notifications":
                elem = practicePageElements.Notifications_button;
                break;

            case "quote templates":
                elem = practicePageElements.Quote_templates_button;
                break;

            case "product list":
                elem = practicePageElements.Product_list_button;
                break;

            case "categories":
                elem = practicePageElements.Categories_button;
                break;

            case "manage inventory":
                elem = practicePageElements.Manage_inventory_button;
                break;

            case "forms library":
                elem = practicePageElements.Forms_library_button;
                break;

            case "scaffold":
                elem = practicePageElements.Scaffold_button;
                break;

            case "blurbs":
                elem = practicePageElements.Blurbs_button;
                break;

            case "drawings manager":
                elem = practicePageElements.Drawings_manager_button;
                break;

            case "external programs":
                elem = practicePageElements.External_programs_button;
                break;

            case "calendar export":
                elem = practicePageElements.Calendar_export_button;
                break;

            case "options":
                elem = practicePageElements.Options_button;
                break;


            case "billing":
                elem = practicePageElements.Billing_button;
                break;

        }

        return Task.where(`{0} clicks element from the expanded dropdown menu in the patients header`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Click.on(elem)
        )
    },


}

export const phonebookElementsActions = {

    enterTextInPhonebookField: (text: string) => Task.where(`{0} enters ${text} in the search field`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(practicePageElements.Phonebook_search_field, Is.visible()),
        Enter.theValue(text).into(practicePageElements.Phonebook_search_field)
    ),

    checkIfPhonebookContactOpened: () => Task.where(`{0} check if phonebook entry opened`,
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(practicePageElements.Phonebook_entry_modal, Is.visible()),
    ),

    clickNewContactButton: () => Task.where(`{0} clicks the new contact button`,
    checkIfLoaderAppears(),    
    waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(practicePageElements.New_contact_button, Is.visible()),
        Scroll.to(practicePageElements.New_contact_button),
        Click.on(practicePageElements.New_contact_button)
    ),

    clickSaveContactButton: () => Task.where(`{0} clicks the save contact button`,
        Wait.upTo(waitTime).until(practicePageElements.Save_contact_button, Is.visible()),
        Scroll.to(practicePageElements.Save_contact_button),
        Click.on(practicePageElements.Save_contact_button)
    ),

    clickPrivateContactCheckbox: () => Task.where(`{0} clicks the private contact checkbox`,
    Wait.upTo(waitTime).until(practicePageElements.Phonebook_private_contact_checkbox, Is.visible()),
    Scroll.to(practicePageElements.Phonebook_private_contact_checkbox),
    Click.on(practicePageElements.Phonebook_private_contact_checkbox)
),


    enterContactName: (text: string) => Task.where(`{0} enters ${text} in the contact name field`,
        checkIfLoaderAppears(),
        Wait.upTo(waitTime).until(practicePageElements.Fist_name_field, Is.visible()),
        Enter.theValue(text).into(practicePageElements.Fist_name_field)
    ),

    enterContactSurname: (text: string) => Task.where(`{0} enters ${text} in the contact surname field`,
        checkIfLoaderAppears(),
        Wait.upTo(waitTime).until(practicePageElements.Last_name_field, Is.visible()),
        Enter.theValue(text).into(practicePageElements.Last_name_field)
    ),

    enterContactOrganization: (text: string) => Task.where(`{0} enters ${text} in the contact organization field`,
        checkIfLoaderAppears(),
        Wait.upTo(waitTime).until(practicePageElements.Organization_field, Is.visible()),
        Enter.theValue(text).into(practicePageElements.Organization_field)
    ),


    clickOnPhonebookContact: (text: string) => {
        let elem = Target.the(`${text} phonebook element`).located(by.xpath(`//div[contains(text(),"${text}")]`))
        return Task.where(`{0} clicks the ${text} contact`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Click.on(elem)
        )
    },

    checkSearchResultsCount: (text: string) => Task.where(`{0} checks if the count is ${text}`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(practicePageElements.Phonebook_search_results_table, Is.visible()),
        Interaction.where(`{0} checks the reutrned results count`, async actor => {
            let count = await BrowseTheWeb.as(james).locate(practicePageElements.Phonebook_search_results_table).getAttribute("childElementCount");
            expect(text).to.eql(count);
        })
    ),
};

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
