import { Scroll, See, Task, Text, Wait, Is, Enter, Target, Interaction, BrowseTheWeb, Click, Duration, Clear } from 'serenity-js/lib/screenplay-protractor';

import { doctorsAndStaffElements } from './ui/doctors-and-staff';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { checkIfLoaderAppears, james } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
import { waitTime } from '../../patients/clinical/medical-history-page-actions';
import { browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';
chai.use(chaiAsPromised);

export const doctorsPageActions = {

    enterTextInDoctorNameField: (text: string) => Task.where(`{0} enters ${text} as the doctor name`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(doctorsAndStaffElements.First_name_field, Is.clickable()),
        Scroll.to(doctorsAndStaffElements.First_name_field),
        Clear.theValueOf(doctorsAndStaffElements.First_name_field),
        Enter.theValue(text).into(doctorsAndStaffElements.First_name_field)
    ),


    saveChangesButton: () => Task.where(`{0} clicks the save button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(doctorsAndStaffElements.Save_doctor_button, Is.clickable()),
        Scroll.to(doctorsAndStaffElements.Save_doctor_button),
        Click.on(doctorsAndStaffElements.Save_doctor_button)

    ),
};

export const doctorsAndStaffActions = {

    clickOnDoctorByName: (text: string) => {
        let elem = Target.the(`${text} doctor`).located(by.xpath(`//div[@class="doctors-section"]//div[contains(text(),"${text}")]/parent::*`))
        return Task.where(`{0} clicks on tbe ${text} doctor`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },

    checkIfDoctorExistsByName: (text: string) => {
        let elem = Target.the(`${text} doctor`).located(by.xpath(`//div[@class="doctors-section"]//div[contains(text(),"${text}")]/parent::*`))
        return Task.where(`{0} clicks on tbe ${text} doctor`,
            checkIfLoaderAppears(),
            waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(elem, Is.clickable()),
        )
    }


};

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
