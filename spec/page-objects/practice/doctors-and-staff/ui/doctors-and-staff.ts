import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const doctorsAndStaffElements = {

// Doctors edit page
   First_name_field : Target.the('First name field').located(by.xpath('//input[@placeholder="First"]')),
   Save_doctor_button : Target.the('Save doctor button').located(by.xpath('//button[@title="Save"]')),

};
