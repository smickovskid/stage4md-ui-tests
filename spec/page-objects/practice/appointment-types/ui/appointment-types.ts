import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const appointmentTypesElements = {

    
    Appointment_name: Target.the('New button').located(by.xpath('//input[@name="AppointmentTypeName"]')),
    Appointment_description: Target.the('New button').located(by.xpath('//input[@name="Description"]')),
    Happening_category: Target.the('New button').located(by.xpath('//option[@value="3"]')),




};
