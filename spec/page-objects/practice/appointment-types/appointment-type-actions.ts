import { Scroll, See, Task, Text, Wait, Is, Enter, Target, Interaction, BrowseTheWeb, Click, Duration } from 'serenity-js/lib/screenplay-protractor';

import { appointmentTypesElements } from './ui/appointment-types';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { checkIfLoaderAppears, james } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
import { waitTime } from '../../patients/clinical/medical-history-page-actions';
import { browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';
chai.use(chaiAsPromised);

export const appointmentTypeActions = {

   
};

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
