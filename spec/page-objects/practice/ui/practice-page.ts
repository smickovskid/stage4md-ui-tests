import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const practicePageElements = {

    // Admin elements
    Admin_button: Target.the('Admin button').located(by.xpath('//a[contains(text(),"Admin")]')),
    Offices_button: Target.the('Offices button').located(by.xpath('//a[contains(text(),"Offices")]')),
    Doctors_and_staff_button: Target.the('Doctors and staff button').located(by.xpath('//a[contains(text(),"Doctors and Staff")]')),
    Appointment_types_button: Target.the('Appointment types button').located(by.xpath('//a[contains(text(),"Appointment Types")]')),
    Referring_doctors_button: Target.the('Referring doctors button').located(by.xpath('//a[contains(text(),"Referring Doctors")]')),
    Notifications_button: Target.the('Notifications button').located(by.xpath('//a[contains(text(),"Notifications")]')),
    Quote_templates_button: Target.the('Quote templates button').located(by.xpath('//a[contains(text(),"Quote Templates")]')),

    // Products elements
    Products_button: Target.the('Products button').located(by.xpath('//a[contains(text(),"Products")]')),
    Product_list_button: Target.the('Products list button').located(by.xpath('//a[contains(text(),"Product List")]')),
    Categories_button: Target.the('Categories button').located(by.xpath('//a[contains(text(),"Categories")]')),
    Manage_inventory_button: Target.the('Manage inventory button').located(by.xpath('//a[contains(text(),"Manage Inventory")]')),

    // Charting elements
    Charting_button: Target.the('Charting button').located(by.xpath('//a[contains(text(),"Charting")]')),
    Forms_library_button: Target.the('Forms library button').located(by.xpath('//a[contains(text(),"Forms Library")]')),
    Scaffold_button: Target.the('Scaffold button').located(by.xpath('//a[contains(text(),"Scaffold")]')),
    Blurbs_button: Target.the('Blurbs button').located(by.xpath('//a[contains(text(),"Blurbs")]')),
    Drawings_manager_button: Target.the('Drawings manager button').located(by.xpath('//a[contains(text(),"Drawings Manager")]')),

    // Integrations elements
    Integrations_button: Target.the('Integrations button').located(by.xpath('//a[contains(text(),"Integrations")]')),
    External_programs_button: Target.the('External programs button').located(by.xpath('//a[contains(text(),"External Programs")]')),
    Calendar_export_button: Target.the('Calendar Export button').located(by.xpath('//a[contains(text(),"Calendar Export")]')),

    // Subscription elements
    Subscriptions_button: Target.the('Subscriptions button').located(by.xpath('//a[contains(text(),"Subscription")]')),
    Options_button: Target.the('Options button').located(by.xpath('//a[contains(text(),"Options")]')),
    Billing_button: Target.the('Billing button').located(by.xpath('//a[contains(text(),"Billing")]')),

    // Phonebook elements
    New_contact_button: Target.the('New contact button').located(by.xpath('//button[@title="New"]')),
    Phonebook_search_field: Target.the('Phonebook search field').located(by.xpath('//input[@value="Enter text to search..."]')),
    Phonebook_search_results_table: Target.the('Phonebook search results field').located(by.xpath('//*[@id="ContactCardView_DXMainTable"]')),
    Phonebook_entry_modal: Target.the('Phonebook entry modal').located(by.xpath('//h4[contains(text(), "Listing")]')),
    Phonebook_private_contact_checkbox: Target.the('Phonebook private contact checkbox').located(by.xpath('//input[@type="checkbox"]')),


    // New phonebook entry elements
    Fist_name_field: Target.the('Phonebook first name field').located(by.xpath('//input[@placeholder="First"]')),
    Last_name_field: Target.the('Phonebook last name field').located(by.xpath('//input[@placeholder="Last"]')),
    Organization_field: Target.the('Phonebook organization field').located(by.xpath('//label[contains(text(),"Organization")]/parent::*//input')),
    Save_contact_button: Target.the('Save contact button').located(by.xpath('//button[@title="Save"]')),











};
