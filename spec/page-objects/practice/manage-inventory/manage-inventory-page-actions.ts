import { Scroll, See, Task, Text, Wait, Is, Enter, Target, Interaction, BrowseTheWeb, Click, Duration } from 'serenity-js/lib/screenplay-protractor';

import { manageInventoryPageElements } from './ui/manage-inventory-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { checkIfLoaderAppears, james } from '../../general';
import { waitForAllTheModalsToBeAbsent } from '../../schedule';
import { waitTime } from '../../patients/clinical/medical-history-page-actions';
import { browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';
chai.use(chaiAsPromised);

export const manageInventoryPageActions = {

    clickNewInventoryChangeButton: () => Task.where(`{0} clicks the new inventory change button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.New_button, Is.clickable()),
        Scroll.to(manageInventoryPageElements.New_button),
        Click.on(manageInventoryPageElements.New_button),
    ),

    selectNewStockOption: () => Task.where(`{0} selects the new stock option`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.Expand_dropdown, Is.clickable()),
        Click.on(manageInventoryPageElements.Expand_dropdown),
        Wait.for(Duration.ofMillis(5000)),
        Interaction.where(`{0} clicks enter`, async actor =>{
            await element(by.xpath(`//select[@class="form-control"]`)).sendKeys(protractor.Key.TAB);
            await element(by.xpath(`//select[@class="form-control"]`)).sendKeys(protractor.Key.ENTER);

        })
    ),

    clickChangeColumn: () => Task.where(`{0} clicks the change column`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.Change_column_button, Is.clickable()),
        Scroll.to(manageInventoryPageElements.Change_column_button),
        Click.on(manageInventoryPageElements.Change_column_button),
    ),

    enterTextInChangeColumnField: (text: string) => Task.where(`{0} enters ${text} in the change column`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.Change_column_text_field, Is.clickable()),
        Scroll.to(manageInventoryPageElements.Change_column_text_field),
        Enter.theValue(text).into(manageInventoryPageElements.Change_column_text_field)
    ),


    clickSaveColumnChangeButton: () => Task.where(`{0} clicks the save column change button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.Save_column_change_button, Is.clickable()),
        Scroll.to(manageInventoryPageElements.Save_column_change_button),
        Click.on(manageInventoryPageElements.Save_column_change_button),
    ),


    clickSaveChangesButton: () => Task.where(`{0} clicks the save changes button`,
        checkIfLoaderAppears(),
        waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(manageInventoryPageElements.Save_changes_button, Is.clickable()),
        Scroll.to(manageInventoryPageElements.Save_changes_button),
        Click.on(manageInventoryPageElements.Save_changes_button),
    ),
};

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
