import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const manageInventoryPageElements = {

    // Admin elements
    New_button: Target.the('New button').located(by.xpath('//div[@class="supply-change-list max-width800"]//button[@title="Add"]')),
    
    
    Expand_dropdown: Target.the('New stock option').located(by.xpath('//*[@id="supply-change-detail"]//select')),

    New_stock_option: Target.the('New stock option').located(by.xpath('//option[contains(text(),"Sold")]')),
    Change_column_button: Target.the('Change column button').located(by.xpath('//tr[@id="Supplies0row"]//td[@class="coltype_number colname_Units"]')),
    Change_column_text_field: Target.the('Change column text field').located(by.xpath('//input[@form="Supplies0"]')),
    Save_column_change_button: Target.the('Save column change button').located(by.xpath('//button[@form="Supplies0"]')),
    Save_changes_button: Target.the('Save changes button').located(by.xpath('//button[@class="btn s4btn s4btn-primary"]')),

};
