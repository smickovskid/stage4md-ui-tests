import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const generalElements = {
    Schedule_Button: Target.the('Schedule button').located(by.xpath('//span[text()="Schedule"]')),
    Patients_Button: Target.the('Patients button').located(by.xpath('//span[text()="Patients"]')),
    Money_Button: Target.the('Money button').located(by.xpath('//span[text()="Money"]')),
    Practice_Button: Target.the('Practice button').located(by.xpath('//span[text()="Practice"]')),
    Background_Loader: Target.the('Background Loader').located(by.xpath('//div[@style="padding-top: 15%; overflow-y: visible; display: block; padding-left: 0px;"]')),
    Vue_Modal: Target.the('Vue modal').located(by.xpath('//div[@id="vueModal"]/div/div')),
    Nav_menu_button: Target.the('nav menu button').located(by.id('NavMenu_DXI4_T')),
    InstAnnounce_loader: Target.the('InstAnnounce loader').located(by.id('ThreadNewestCardView_LPV')),
    InstAnnounce_loader2: Target.the('InstAnnounce loader').located(by.id('Staffonlinelist_LPV')),

    

    // profile elements
    InstAnnounce_button: Target.the('InstAnnounce button').located(by.xpath('//span[contains(text(), "InstAnn")]')),

    // Instannounce modal elements
    New_message_button: Target.the('New message button').located(by.id('NewInstannounce')),
    New_message_text_feild: Target.the('New message text field').located(by.id('instamessage')),
    New_message_to_everyone_button: Target.the('New message to everyone button').located(by.id('everyoneIAbtn')),
    Send_message_button: Target.the('Send message button').located(by.xpath('//button[@onclick="SendInstannounceBtn()"]')),




};
