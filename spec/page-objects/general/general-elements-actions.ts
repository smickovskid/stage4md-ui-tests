import { Scroll, See, Task, Text, Wait, Is, Enter, Duration, Click, Interaction, Target, BrowseTheWeb, Actor } from 'serenity-js/lib/screenplay-protractor';

import { generalElements } from './ui/general-elements';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
import { protractor } from 'protractor/built/ptor';
import { browser, element, by, ElementFinder } from 'protractor';
import { checkIfScheduleLoaderAppears, waitForAllTheModalsToBeAbsent } from '../schedule';
import { moneyPageElements } from '../money/ui/money-page';
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);

export const  james = Actor.named("james").whoCan(BrowseTheWeb.using(browser));

export const generalElementsActions = {
    // checkIfLoaderAppears() must be called since the loader sometimes takes a second to appear
    // This causes an error that the element can't be clicked


    hardWait: (milis: number) => Task.where(`{0} waits for ${milis} milis`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.for(waitTime)
    ),
    clickTheScheduleButton: () => Task.where(`{0} clicks the schedule button`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Schedule_Button, Is.clickable()),
        Scroll.to(generalElements.Schedule_Button),
        Click.on(generalElements.Schedule_Button)
    ),

    clickThePatientsButton: () => Task.where(`{0} clicks the patients button`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Patients_Button, Is.clickable()),
        Scroll.to(generalElements.Patients_Button),
        Click.on(generalElements.Patients_Button)
    ),

    waitForInstAnnounceLoaderToDisappear: () => Task.where(`{0} wait for instannounce loader to disappear`,
    Wait.upTo(waitTime).until(generalElements.InstAnnounce_loader, Is.visible()),
    Wait.upTo(waitTime).until(generalElements.InstAnnounce_loader2, Is.visible()),

    Wait.upTo(waitTime).until(generalElements.InstAnnounce_loader, Is.absent()),
    Wait.upTo(waitTime).until(generalElements.InstAnnounce_loader2, Is.absent()),
),

    clickTheInstAnnounceButton: () => Task.where(`{0} clicks the InstAnnounce button`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.InstAnnounce_button, Is.clickable()),
        Scroll.to(generalElements.InstAnnounce_button),
        Click.on(generalElements.InstAnnounce_button)
    ),


    clickTheNewMessageButton: () => Task.where(`{0} clicks the new message button`,
     Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.absent()),
        Wait.upTo(waitTime).until(generalElements.New_message_button, Is.clickable()),
        Scroll.to(generalElements.New_message_button),
        Click.on(generalElements.New_message_button)
    ),

    clickSendMessageButton: () => Task.where(`{0} clicks the send message button`,
        Wait.upTo(waitTime).until(generalElements.Send_message_button, Is.clickable()),
        Scroll.to(generalElements.Send_message_button),
        Click.on(generalElements.Send_message_button)
    ),

    clickMessageToEveryoneButton: () => Task.where(`{0} clicks the message to everyone button`,
        Wait.upTo(waitTime).until(generalElements.New_message_to_everyone_button, Is.clickable()),
        Scroll.to(generalElements.New_message_to_everyone_button),
        Click.on(generalElements.New_message_to_everyone_button)
    ),

    enterTextInMessageFiled: (text: string) => Task.where(`{0} enters text in the new message field`,
        Wait.upTo(waitTime).until(generalElements.New_message_text_feild, Is.visible()),
        Enter.theValue(text).into(generalElements.New_message_text_feild)
    ),

    clickTheMoneyButton: () => Task.where(`{0} clicks the money button`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Money_Button, Is.clickable()),
        Scroll.to(generalElements.Money_Button),
        Click.on(generalElements.Money_Button)
    ),

    clickThePracticeButton: () => Task.where(`{0} clicks the practice button`,
        checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
        Wait.upTo(waitTime).until(generalElements.Practice_Button, Is.clickable()),
        Scroll.to(generalElements.Practice_Button),
        Click.on(generalElements.Practice_Button),
    ),

    waitForLoaderToDisappear: () => Task.where(`{0} waits for the loader to disappear`,
        Wait.upTo(waitTime).until(moneyPageElements.Shopping_cart_loader, Is.invisible()),
    ),


    waitForShoppingCartLoaderToDisappear: () => Task.where(`{0} waits for the loader to disappear`,
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
    ),

    waitForVueModalToDisappear: () => Task.where(`{0} waits for the vue modal to disappear`,
        Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
    ),


    checkIfMessageWasSent: (text: string) => {
        let elem = Target.the(`${text} message`).located(by.xpath(`//div[contains(text(),"${text}")]`))
        return Task.where(`{0} checks if the message ${text} was sent`,
            Wait.upTo(waitTime).until(elem, Is.visible()),

        )
    },

    hoverOverProfileButton: () => {
        return Task.where(`{0} hovers over the profile button`,
            checkIfScheduleLoaderAppears(), waitForAllTheModalsToBeAbsent(),
            Wait.upTo(waitTime).until(generalElements.Background_Loader, Is.invisible()),
            Wait.upTo(waitTime).until(generalElements.Vue_Modal, Is.invisible()),
            Wait.upTo(waitTime).until(generalElements.Nav_menu_button, Is.clickable()),

            Interaction.where(
                `hovers over the profile button`,
                async actor => {
                    await browser.actions().mouseMove(BrowseTheWeb.as(james).locate(generalElements.Nav_menu_button)).mouseDown().perform();
                })
        )
    }

}
export const checkIfLoaderAppears = () =>
    Interaction.where(
        `#checks if the loader has appeared`,
        async actor => {
            let loader = element(by.xpath('//div[@style="padding-top: 15%; overflow-y: visible; display: block; padding-left: 0px;"]'));
            var until = protractor.ExpectedConditions;
            await browser.wait(until.presenceOf(loader), 1500, "Didn't appear").catch(err => {
                return;
            })

        })

export const checkIfCartLoaderAppears = () =>
    Interaction.where(
        `#checks if the loader has appeared in the shopping cart`,
        async actor => {
            let loader = element(by.id('CartRegisterGrid_LPV'));
            var until = protractor.ExpectedConditions;
            await browser.wait(until.presenceOf(loader), 3000, "Didn't appear").catch(err => {
                return;
            })

        })


const expect = chai.expect;
// assertions
export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
