import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by } from 'protractor';

export const loginPage = {

    // Login form elements
    Email_Field: Target.the('Email field').located(by.id('Email')),
    Password_Field: Target.the('Password field').located(by.id('Password')),
    Login_Button: Target.the('Login button').located(by.id('Loginbtn')),
    
    // Forgot password elements
    Forgot_Password_Button: Target.the('Forgot password button').located(by.xpath('//a[@href="/Account/ForgotPassword"]')),
    Forgot_Password_Email_Field: Target.the('Forgot password email field').located(by.css('form[action="/Account/ForgotPassword"] >* input')),
    Send_Email_Button: Target.the('Send email button').located(by.xpath('//button[text()="Send Email Link"]')),

};
