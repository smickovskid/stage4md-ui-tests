import { Scroll, See, Task, Text, Wait, Is, Enter, Duration, Click } from 'serenity-js/lib/screenplay-protractor';

import { loginPage } from './ui/login-page';

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let waitTime = Duration.ofMillis(30000);


export const loginPageActions = {
/**
 * Enters the provided string to the email login field
 */
    enterEmail: (email: string) => Task.where(`{0} enters his email as "${email}"`,
        Wait.upTo(waitTime).until(loginPage.Email_Field, Is.visible()),
        Scroll.to(loginPage.Email_Field),
        Enter.theValue(email).into(loginPage.Email_Field)
    ),

/**
* Enters the provided string to the password login field
*/
    enterPassword: (password: string) => Task.where(`{0} enters his password as "${password}"`,
        Wait.upTo(waitTime).until(loginPage.Password_Field, Is.visible()),
        Scroll.to(loginPage.Password_Field),
        Enter.theValue(password).into(loginPage.Password_Field)
    ),

/**
* Clicks on the log in button.
* Email and password need to be previoulsy provided if a happy path scenario is being executed
*/
    clickTheLoginButton: () => Task.where(`{0} clicks the login button`,
        Wait.upTo(waitTime).until(loginPage.Login_Button, Is.clickable()),
        Scroll.to(loginPage.Login_Button),
        Click.on(loginPage.Login_Button)
    ),
/**
* Clicks on the forgot password button
*/
    clickForgotPasswordButton: () => Task.where(`{0} clicks the forgot password button`,
        Wait.upTo(waitTime).until(loginPage.Forgot_Password_Button, Is.clickable()),
        Scroll.to(loginPage.Forgot_Password_Button),
        Click.on(loginPage.Forgot_Password_Button)
    ),

/**
* Enters the provided string in the forgot password field
*/ 
    enterPasswordInForgotPasswordField: (email: string) => Task.where(`{0} enters his email in the forgot password field as "${email}"`,
        Wait.upTo(waitTime).until(loginPage.Forgot_Password_Email_Field, Is.visible()),
        Scroll.to(loginPage.Forgot_Password_Email_Field),
        Enter.theValue(email).into(loginPage.Forgot_Password_Email_Field)
    ),

/**
* Clicks on the send email button for forgot password
*/ 
    clickSendEmailButton: () => Task.where(`{0} clicks the send email button`,
        Wait.upTo(waitTime).until(loginPage.Send_Email_Button, Is.clickable()),
        Scroll.to(loginPage.Send_Email_Button),
        Click.on(loginPage.Send_Email_Button)
    ),
}

// assertions

const expect = chai.expect;

export const equals = (expected: string) => (actual: PromiseLike<string>) => expect(actual).to.eventually.equal(expected)
