import { protractor, browser } from 'protractor';
import { serenity } from 'serenity-js';
import { Actor, BrowseTheWeb, Open } from 'serenity-js/lib/screenplay-protractor';

import { schedulePageActions, scheduleContextMenuOptions, scheduleHappeningModalActions, newPatientAppointmentActions, existingPatientAppointmentActions } from './page-objects/schedule';
import { loginPageActions } from './page-objects/login';
import { generalElementsActions } from './page-objects/general';
import { patientsPageActions, profilePageActions, ledgerPageActions, summaryPageActions } from './page-objects/patients';
import { quotesPageActions, newQuoteActons } from './page-objects/patients/quotes-page-actions';
import { newQuotesElements } from './page-objects/patients/ui/quotes-page';
import { writeNotesPageActions } from './page-objects/patients/clinical/write-notes-page-actions';
import { readNotesPageActions } from './page-objects/patients/clinical/read-notes-page-actions';
import { medicalHistoryPageActions } from './page-objects/patients/clinical/medical-history-page-actions';
import { moneyPageActions, emailFormActions } from './page-objects/money';
import { practicePageActions, phonebookElementsActions } from './page-objects/practice';
import { manageInventoryPageActions } from './page-objects/practice/manage-inventory';
import { doctorsAndStaffActions, doctorsPageActions } from './page-objects/practice/doctors-and-staff';
var randomstring = require("randomstring");
var id;
// describe represents a suite
describe('Stage4md Scenarios ->', function () {
    this.timeout(600 * 1000);

    const stage = serenity.callToStageFor({
        actor: name => Actor.named(name).whoCan(BrowseTheWeb.using(protractor.browser)),
    });

    before(() => {
        // browser.driver.manage().window().maximize();
        id = randomstring.generate({
            length: 10,
            charset: 'alphabetic'
        });
        browser.driver.manage().window().maximize();
    })

    afterEach(async () => {
        await browser.manage().deleteAllCookies();
        await browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();')
    })
    beforeEach(() => {

        return stage.theActorInTheSpotlight().attemptsTo(
            Open.browserOn("https://qa.stage4md.com"),
        )
    });


    // it('S4M-429 | [InstAnnounce] User tries to send new message', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.hoverOverProfileButton(),
    //         generalElementsActions.clickTheInstAnnounceButton(),
    //         generalElementsActions.waitForInstAnnounceLoaderToDisappear(),
    //         generalElementsActions.clickTheNewMessageButton(),
    //         generalElementsActions.enterTextInMessageFiled(id),
    //         generalElementsActions.clickMessageToEveryoneButton(),
    //         generalElementsActions.clickSendMessageButton(),
    //         generalElementsActions.checkIfMessageWasSent(id)
    //     ));

    // it('S4M-424 | [Phonebook] User tries to search phonebook', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.enterTextInPhonebookField("James"),
    //         phonebookElementsActions.checkSearchResultsCount("1")
    //     ));


    // it('S4M-423 | [Phonebook] User tries to view existing entry from phonebook', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.clickOnPhonebookContact("James"),
    //         phonebookElementsActions.checkIfPhonebookContactOpened()
    //     ));


    // it('S4M-423 | [Phonebook] User tries to view existing entry from phonebook', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.clickOnPhonebookContact("James"),
    //         phonebookElementsActions.checkIfPhonebookContactOpened()
    //     ));

    // it('S4M-422 | [Phonebook] User creates new entry in phonebook', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.clickNewContactButton(),
    //         phonebookElementsActions.enterContactName(id),
    //         phonebookElementsActions.enterContactSurname(id),
    //         phonebookElementsActions.enterContactOrganization(id),
    //         phonebookElementsActions.clickSaveContactButton(),
    //         phonebookElementsActions.enterTextInPhonebookField(id),
    //         phonebookElementsActions.checkSearchResultsCount("1")
    //     ));


    // it('S4M-410 |[Quotes] User tries to create new quote with procedure that has supplies', () => {
    //     return stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePatientsButton(),
    //         patientsPageActions.enterTextInSearchField("Damjanzzsad"),
    //         patientsPageActions.clickOnQuotesButton(),
    //         quotesPageActions.setQuoteCount(),
    //         quotesPageActions.clickNewQuoteButton(),
    //         newQuoteActons.selectProvider("James"),
    //         newQuoteActons.clickAddProcedureButton(),
    //         newQuoteActons.expandProcedureButton(),
    //         newQuoteActons.selectProcedureFromDropdown("Breast Reduction"),
    //         newQuoteActons.clickAddSupplyButton(),
    //         newQuoteActons.expandSuppliesButton(),
    //         newQuoteActons.selectSupplyFromDropdown("Botox"),
    //         newQuoteActons.clickSaveButton(),
    //         quotesPageActions.checkQuotecount()

    //     )
    // });

    // it('S4M-403 | [Schedule] User tries to go to patient from appointment', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.filterSchedule("James"),
    //         schedulePageActions.rightClickOnCalendar("10am"),
    //         scheduleContextMenuOptions.clickTheExistingPatientButton(),
    //         existingPatientAppointmentActions.enterPatientNameInSearchBar("Damjanzzsad"),
    //         existingPatientAppointmentActions.clickOnAppointmentTypeDropdown(),
    //         existingPatientAppointmentActions.selectAppointmentType("Surgery"),
    //         existingPatientAppointmentActions.clickSaveAppointmentButton(),
    //         schedulePageActions.checkIfNewPatientAppointmentWasCreated("Damjanzzsad"),
    //         schedulePageActions.rightClickOnAppointment("Damjanzzsad"),
    //         schedulePageActions.hoverOverContextMenuButton("patient"),
    //         schedulePageActions.clickButtonFromContextMenuExpanded("Go To Patient"),
    //         generalElementsActions.hardWait(5000)

    //     ));

    // //Duplicate of S4M-423
    // it('S4M-334 | [Contacts] User tries to open contact details', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.clickOnPhonebookContact("James"),
    //         phonebookElementsActions.checkIfPhonebookContactOpened()
    //     ));


    // //TODO Needs logic to select from native browser dropdown, scenario has been implemented
    // it('S4M-387 | [Manage Inventory] User edits tracked item from inventory', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         practicePageActions.hoverOverTabButton("Products"),
    //         practicePageActions.clickButtonFromDropdownMenu("Manage Inventory"),
    //         manageInventoryPageActions.clickNewInventoryChangeButton(),
    //         manageInventoryPageActions.selectNewStockOption(),
    //         manageInventoryPageActions.clickChangeColumn(),
    //         manageInventoryPageActions.enterTextInChangeColumnField("5"),
    //         manageInventoryPageActions.clickSaveColumnChangeButton(),
    //         manageInventoryPageActions.clickSaveChangesButton(),

    //     ));


    // it('S4M-196 | [Schedule] User copies appointment', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.filterSchedule("James"),
    //         schedulePageActions.rightClickOnCalendar("7am"),
    //         scheduleContextMenuOptions.clickTheHappeningButton(),
    //         scheduleHappeningModalActions.enterHappeningSubject(id),
    //         scheduleHappeningModalActions.enterHappeningComments(id),
    //         scheduleHappeningModalActions.clickSaveHappeningButton(),
    //         schedulePageActions.checkIfHappeningWasCreated(id),
    //         schedulePageActions.rightClickOnAppointment(id),
    //         schedulePageActions.clickCopyButtonContextMenu(),
    //         schedulePageActions.rightClickOnCalendar("11am"),
    //         schedulePageActions.clickPasteButtonContextMenu(),
    //         schedulePageActions.checkIfHappeningWasCreated(id)
    //     ));

    // //it('S4M-369 | [Appointment types] User tries to create a new appointment type', () =>



    // it('S4M-363 | [Doctors & Staff] User tries to edit existing doctor', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         practicePageActions.hoverOverTabButton("Admin"),
    //         practicePageActions.clickButtonFromDropdownMenu("Appointment Types"),

    //     ));


    // it('S4M-226 | [Summary] User navigates to allergies', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         practicePageActions.hoverOverTabButton('Admin'),
    //         practicePageActions.clickButtonFromDropdownMenu('doctors and staff'),
    //         doctorsAndStaffActions.clickOnDoctorByName("Cvetan"),
    //         doctorsPageActions.enterTextInDoctorNameField(`Cvetan - ${id}`),
    //         doctorsPageActions.saveChangesButton(),
    //         doctorsAndStaffActions.checkIfDoctorExistsByName(`Cvetan - ${id}`)
    //     ));

    // it('S4M-333 | [Contacts] User tries to create new private contact', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePracticeButton(),
    //         phonebookElementsActions.clickNewContactButton(),
    //         phonebookElementsActions.enterContactName(id),
    //         phonebookElementsActions.enterContactSurname(id),
    //         phonebookElementsActions.enterContactOrganization(id),
    //         phonebookElementsActions.clickPrivateContactCheckbox(),
    //         phonebookElementsActions.clickSaveContactButton(),
    //         phonebookElementsActions.enterTextInPhonebookField(id),
    //         phonebookElementsActions.checkSearchResultsCount("1")
    //     ));



    // it('S4M-284 | [Write notes] User tries to remove note from appointment', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         generalElementsActions.clickThePatientsButton(),
    //         patientsPageActions.enterTextInSearchField("Damjanka"),
    //         summaryPageActions.clickOnAllergiesButton(),
    //         summaryPageActions.checkIfMedicalPageOpenet()

    //     ));


    // it('S4M-220 | [Schedule] User views appointments summary', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('damjan.smickovski+9@toptal.com'),
    //         loginPageActions.enterPassword('Damjanka123'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.rightClickOnCalendar("4pm"),
    //         scheduleContextMenuOptions.clickTheHappeningButton(),
    //         scheduleHappeningModalActions.enterHappeningSubject(id),
    //         scheduleHappeningModalActions.enterHappeningComments(id),
    //         scheduleHappeningModalActions.clickSaveHappeningButton(),
    //         schedulePageActions.clickOnAppointment(id),
    //         schedulePageActions.checkIfAppointmentSummaryOpened()
    //     ));


    // it('S4M-217 | [Schedule] User tries to show inactive meeting', () =>
    //         patientsPageActions.hoverOverTabButton('Clinical'),
    //         patientsPageActions.clickButtonFromDropdownMenu('Write notes'),
    //         writeNotesPageActions.clickDeleteNoteFromAppointment(),
    //     ));



    // schedulePageActions.filterSchedule("James"),
    // schedulePageActions.rightClickOnCalendar("10am"),
    // scheduleContextMenuOptions.clickTheExistingPatientButton(),
    // existingPatientAppointmentActions.enterPatientNameInSearchBar("Damjanzzsad"),
    // existingPatientAppointmentActions.clickOnAppointmentTypeDropdown(),
    // existingPatientAppointmentActions.selectAppointmentType("Surgery"),
    // existingPatientAppointmentActions.clickSaveAppointmentButton(),
    // schedulePageActions.checkIfNewPatientAppointmentWasCreated("Damjanzzsad"),
    // schedulePageActions.rightClickOnAppointment("Damjanzzsad"),
    // schedulePageActions.hoverOverContextMenuButton("reschedule"),
    // schedulePageActions.clickRescheduleByOffice(),
    // schedulePageActions.rightClickOnCalendar("5pm"),
    // schedulePageActions.clickPasteButtonContextMenu()
    // ));

    // it('S4M-217 | [Schedule] User tries to show inactive meeting', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.filterSchedule("James"),
    //         schedulePageActions.rightClickOnCalendar("11am"),
    //         scheduleContextMenuOptions.clickTheHappeningButton(),
    //         scheduleHappeningModalActions.enterHappeningSubject(id),
    //         scheduleHappeningModalActions.enterTextInAppointmentTypeField("hap"),
    //         scheduleHappeningModalActions.enterHappeningComments(id),
    //         scheduleHappeningModalActions.clickSaveHappeningButton(),
    //         schedulePageActions.checkIfHappeningWasCreated(id),
    //         schedulePageActions.rightClickOnAppointment(id),
    //         schedulePageActions.hoverOverContextMenuButton('Cancel'),
    //         schedulePageActions.clickButtonFromContextMenuExpanded('cancel by office'),
    //         schedulePageActions.clickShowInactiveButton(),
    //         schedulePageActions.checkIfHappeningWasCreated(id),
    //     ));


    // it('S4M-198 | [Schedule] User cancels appointment by office', () =>
    //     stage.theActorInTheSpotlight().attemptsTo(
    //         loginPageActions.enterEmail('demo01@stage4md.com'),
    //         loginPageActions.enterPassword('demo01password'),
    //         loginPageActions.clickTheLoginButton(),
    //         schedulePageActions.rightClickOnCalendar("10am"),
    //         scheduleContextMenuOptions.clickTheExistingPatientButton(),
    //         existingPatientAppointmentActions.enterPatientNameInSearchBar("Damjanka"),
    //         scheduleHappeningModalActions.enterTextInAppointmentTypeField("Sur"),
    //         existingPatientAppointmentActions.clickSaveAppointmentButton(),
    //         schedulePageActions.checkIfNewPatientAppointmentWasCreated("Damjanka"),
    //         schedulePageActions.rightClickOnAppointment("Damjanka"),
    //         schedulePageActions.hoverOverContextMenuButton("cancel"),
    //         schedulePageActions.clickButtonFromContextMenuExpanded("cancel by office"),
    //         schedulePageActions.checkIfAppointmentIsAbsent("Damjanka")
    //     ));


        // it('S4M-230 | [Summary] User views patient profile', () =>
        // stage.theActorInTheSpotlight().attemptsTo(
        //     loginPageActions.enterEmail('demo01@stage4md.com'),
        //     loginPageActions.enterPassword('demo01password'),
        //     loginPageActions.clickTheLoginButton(),
        //     generalElementsActions.clickThePatientsButton(),
        //     patientsPageActions.enterTextInSearchField("Damjanka"),
        //     summaryPageActions.clickOnViewProfileButton(),
        //     profilePageActions.checkIfProfileOpeneed()

        // ));

        it('S4M-264 | [Read notes] User reads all notes', () =>
        stage.theActorInTheSpotlight().attemptsTo(
            loginPageActions.enterEmail('demo01@stage4md.com'),
            loginPageActions.enterPassword('demo01password'),
            loginPageActions.clickTheLoginButton(),
            generalElementsActions.clickThePatientsButton(),
            patientsPageActions.enterTextInSearchField("Damjanka"),
            patientsPageActions.hoverOverTabButton("clinical"),
            patientsPageActions.clickButtonFromDropdownMenu("read notes"),
            readNotesPageActions.clickReadAllNotesButton()

        ));

});