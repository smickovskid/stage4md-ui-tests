# Stage4MD UI Tests

### **Pre-requisites**
- JRE (any)
- NodeJS >= v10.15.2 ([Windows Download link](https://nodejs.org/en/download/), [UNIX based OS instructions](https://nodesource.com/blog/installing-node-js-tutorial-using-nvm-on-mac-os-x-and-ubuntu/))
- NPM >= v6.4.1 (installed alongside with NodeJS)
- Protractor >= v5.4.2 (run `npm install -g protractor` - requires admin or sudo priviliges)

### **Structure and hierarchy**
1. **Spec files `spec/*.spec.ts`** - Top level files that contain the scenarios and wrapped in describe() and it() blocks. (Mocha used as an executor)
2. **Page object action files `page-objects/{pagename}/{pagename}-page-actions.ts}`** - Layer 2 files that contain the actions/methods and assertions for elements for each given page
3. **Element locator files `page-bjects/{pagename}/ui/{pagename}-page.ts`** - Layer 3 files that serve as a centralized location of element locators for each page
4. **Target folder** - Created after executing a scenario(s) which generates an html report that can be found at `target/site/serenity/index.html`
5. **Protractor configuration file `protractor.conf.js`** - Contains the configurations of the execution environment

### How to run any scenario(s)
1. Clone this repo
2. run `npm install` in this folder
3. run `npm run local` - this will run all the spec files in the spec folder
4. run `npm run report` - this will generate a report

### Notes
- The headless option can be added/removed in the `protractor.conf.js` file
- When creating a method that does something on a page, it is important to call a function like waitForAllTheModalsToBeAbsent() so the framework checks for any modals or loaders before clicking or doing an action

### Documentation
- [Nodejs](https://nodejs.org/en/docs/)
- [NPM](https://docs.npmjs.com/)
- [Protractor](https://www.protractortest.org/#/api)
- [SerenityJS](https://serenity-js.org/)
- [MochaJS](https://mochajs.org/)
- [ChaiJS](https://www.chaijs.com/)