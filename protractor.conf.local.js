const
    glob = require('glob'),
    protractor = require.resolve('protractor'),
    node_modules = protractor.substring(0, protractor.lastIndexOf('node_modules') + 'node_modules'.length),
    seleniumJar = glob.sync(`${node_modules}/protractor/**/selenium-server-standalone-*.jar`).pop();

const crew = require('serenity-js/lib/stage_crew');


exports.config = {

    onPrepare: async function () {
        const capabilities = await browser.getCapabilities();
        console.log(capabilities.get('version'));
        browser.ignoreSynchronization=true;

        //  browser.driver.manage().window().maximize();
        by.addLocator("cssSingleElement", (marker, parentElement, indexOfElement) => {
            indexOfElement = indexOfElement || 0;
            const scope = parentElement || document;
            console.log(document);
            const matches = scope.querySelectorAll(marker);
            if (matches.length === 0) {
                return null;
            } else if (matches.length === 1) {
                return matches[indexOfElement];
            }
            return matches[indexOfElement];
        });

        by.addLocator("xpathSingleElement", (marker, parentElement, indexOfElement) => {
            const scope = parentElement || document;
            indexOfElement = indexOfElement || 1;
            let singleMatch;
            const matches = scope.evaluate(marker, document);
            for (let i = 0; i < indexOfElement; i++)
                singleMatch = matches.iterateNext();
            if (singleMatch != null)
                return singleMatch;

            else
                return null;
        });
    },



    seleniumServerJar: seleniumJar,

    // https://github.com/angular/protractor/blob/master/docs/timeouts.md
    allScriptsTimeout: 110000,

    framework: 'custom',
    frameworkPath: require.resolve('serenity-js'),

    specs: ['spec/**/*.spec.ts'],

    mochaOpts: {
        ui: 'bdd',
        compiler: 'ts:ts-node/register',
        //bail: true


    },
    serenity: {
        crew: [
            crew.serenityBDDReporter(),
            //crew.consoleReporter(),
            crew.Photographer.who(_ => _
                .takesPhotosOf(_.Failures)//.takesPhotosWhen(_.Failures)
            )
        ],

        dialect: 'mocha',  // or 'mocha'
    },
    capabilities: {


        browserName: 'chrome',
        chromeOptions: {
 
            prefs: {
                download: {
                    'prompt_for_download': false,
                    'directory_upgrade': true,
                    'default_directory': `${__dirname}/../`
                }
            },
            args: [
                '--disable-infobars',
                '--kiosk-printing',
                 //"--headless",
                 "--disable-gpu",
                 "--no-sandbox",
                "--window-size=1900x1024",
                // "--allow-insecure-localhost",
                // "--remote-debugging-port=9222"
            ]
        }
    },


};